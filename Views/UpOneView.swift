//
//  UpOneView.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 20/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class UpOneView: UIView {

    var localizer = LocalizationManager.shared
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }

    func configure() {
        
        localize()
        
        let upOneImageView = UIImageView()
        
        addSubview(upOneImageView)
        upOneImageView.translatesAutoresizingMaskIntoConstraints = false
        upOneImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        upOneImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        upOneImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        upOneImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        upOneImageView.clipsToBounds = true
        upOneImageView.contentMode = .scaleAspectFill
        upOneImageView.image = UIImage(named: "Slika 1")
        upOneImageView.isUserInteractionEnabled = true
        
        let label = UILabel()
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        label.text = localizer[Labels.firstView]
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
 
    }
    
    fileprivate func localize() {
        let defaults = UserDefaults.standard
        let locale = defaults.string(forKey: "locale")
        if let language = Language(rawValue: locale ?? "English") {
            localizer.changeLanguage(language)
        }
    }
    


}

