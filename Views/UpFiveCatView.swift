//
//  UpFiveCatView.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 21/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class UpFiveCatView: UIView {
    
    var localizer = LocalizationManager.shared
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }
    
    func configure() {
        
        
        self.backgroundColor = UIColor(hexString: "#121925")
        let nameCat = UILabel()
        
        addSubview(nameCat)
        nameCat.translatesAutoresizingMaskIntoConstraints = false
        nameCat.text = localizer[Labels.shopping]
        nameCat.textColor = .white
        nameCat.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nameCat.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -20).isActive = true
        
        let oneCatImageView = UIImageView()
        oneCatImageView.translatesAutoresizingMaskIntoConstraints = false
        oneCatImageView.image = UIImage(named: "kupovina")?.withRenderingMode(.alwaysTemplate)
        oneCatImageView.tintColor = UIColor(hexString: "#DC3C32")
        addSubview(oneCatImageView)
        oneCatImageView.topAnchor.constraint(equalTo: nameCat.bottomAnchor, constant: 5).isActive = true
        oneCatImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        oneCatImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        oneCatImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        oneCatImageView.contentMode = .scaleAspectFit
        oneCatImageView.clipsToBounds = true
        
    }
    
    
    
}
