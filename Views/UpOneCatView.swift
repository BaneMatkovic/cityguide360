//
//  UpOneCatView.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 20/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class UpOneCatView: UIView {

    var localizer = LocalizationManager.shared
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }
    
    func configure() {
        
        localize()
        
        self.backgroundColor = UIColor(hexString: "#121925")
        let nameCat = UILabel()
        
        addSubview(nameCat)
        nameCat.translatesAutoresizingMaskIntoConstraints = false
        nameCat.text = localizer[Labels.aerial]
        nameCat.textColor = .white
        nameCat.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nameCat.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -20).isActive = true
        
        let oneCatImageView = UIImageView()
        oneCatImageView.translatesAutoresizingMaskIntoConstraints = false
        oneCatImageView.image = UIImage(named: "zracna")?.withRenderingMode(.alwaysTemplate)
        oneCatImageView.tintColor = UIColor(hexString: "#1D33E1")
        addSubview(oneCatImageView)
        oneCatImageView.topAnchor.constraint(equalTo: nameCat.bottomAnchor, constant: 5).isActive = true
        oneCatImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        oneCatImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        oneCatImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        oneCatImageView.contentMode = .scaleAspectFit
        oneCatImageView.clipsToBounds = true
        
        
        
        
    }
    
    fileprivate func localize() {
        let defaults = UserDefaults.standard
        let locale = defaults.string(forKey: "locale")
        if let language = Language(rawValue: locale ?? "English") {
            localizer.changeLanguage(language)
        }
    }
    

}
