//
//  upTwoView.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 20/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class UpTwoView: UIView {

    var localizer = LocalizationManager.shared
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }
    
    func configure() {
        
        let upTwoImageView = UIImageView()
        
        addSubview(upTwoImageView)
        upTwoImageView.translatesAutoresizingMaskIntoConstraints = false
        upTwoImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        upTwoImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        upTwoImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        upTwoImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        upTwoImageView.clipsToBounds = true
        upTwoImageView.contentMode = .scaleAspectFill
        upTwoImageView.image = UIImage(named: "Slika 3")
        upTwoImageView.isUserInteractionEnabled = true
        
        let label = UILabel()
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        label.text = localizer[Labels.populare]
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
    }

}
