//
//  UpThreeView.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 20/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class UpThreeView: UIView {

    var localizer = LocalizationManager.shared
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }
    
    func configure() {
        
        let upThreeView = UIImageView()
        
        addSubview(upThreeView)
        upThreeView.translatesAutoresizingMaskIntoConstraints = false
        upThreeView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        upThreeView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        upThreeView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        upThreeView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        upThreeView.clipsToBounds = true
        upThreeView.contentMode = .scaleAspectFill
        upThreeView.image = UIImage(named: "Slika 2")
        upThreeView.isUserInteractionEnabled = true
        
        let label = UILabel()
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        label.text = localizer[Labels.streets]
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        
    }

}
