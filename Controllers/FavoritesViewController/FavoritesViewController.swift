//
//  FavoritesViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 19/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

class FavoritesViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var favoritesList: [Favorites] = []
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var localizer = LocalizationManager.shared
    
    var pomPanoramaId = ""
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadItems()
        
        titleLabel.text = localizer[Buttons.myFavourites]
        
        //self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = .zero
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
     
    }
    
    func loadItems() {
        
        let request: NSFetchRequest<Favorites> = Favorites.fetchRequest()
        
        do {
            //favoritesList = try context.fetch(request)
            
            let list = try context.fetch(request)
            let uniqueValues = list.filterDuplicate{($0.name, $0.panoramaId, $0.imageUrl)}
            favoritesList = uniqueValues
            
        } catch {
            print(error)
        }
        tableView.reloadData()
    }
    
    // MARK: Apperance
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: Actions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
   
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "favoritesToDetail" {
            let destinationVc = segue.destination as! DetailWebViewController
            destinationVc.panoramaId = pomPanoramaId
        }
    }
    

}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("favoritesList count:", favoritesList.count)
        return favoritesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavoritesTableViewCell
        
        let item = favoritesList[indexPath.row]
        
        //cell.textLabel?.text = item.name
        
        
        cell.cellNameLabel.text = item.name
        if let pictureUrl = URL(string: item.imageUrl ?? "https://imagery.cityguide360.ba/thumbs/fcb5606d-ff60-4710-8a86-7a1724c7536e.jpg") {
        let placeholder = UIImage(named: "Slika 1")
        cell.cellImageView.sd_setImage(with: pictureUrl, placeholderImage: placeholder)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pomPanoramaId = favoritesList[indexPath.row].panoramaId ?? ""
        print("sel", pomPanoramaId)
        self.performSegue(withIdentifier: "favoritesToDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let AppDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let Context = AppDel.persistentContainer.viewContext
        
        if editingStyle == .delete {
            context.delete(favoritesList[indexPath.row] as NSManagedObject)
            favoritesList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
        
        var error:Error? = nil
        
        do {
            try Context.save()
        }
        catch let error1 as Error {
            error = error1
            print("greska:",error as Any)
            abort()
        }
        
    }
    
    
}

//MARK: - Search bar methods
extension FavoritesViewController: UISearchBarDelegate {
    
    fileprivate func queryDatabase(_ searchBar: UISearchBar) {
        print("here")
        let request : NSFetchRequest<Favorites> = Favorites.fetchRequest()
        
        let predicate = NSPredicate(format: "name CONTAINS[cd] %@", searchBar.text!)
        //https://academy.realm.io/posts/nspredicate-cheatsheet/
        request.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        request.sortDescriptors = [sortDescriptor]
        
        do {
            favoritesList = try context.fetch(request)
        } catch {
            print(error)
        }
        
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        queryDatabase(searchBar)
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("here2")
        if searchBar.text?.count == 0 {
            loadItems()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        } else {
            queryDatabase(searchBar)
        }
    }
    
    
}
