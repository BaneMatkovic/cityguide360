//
//  Photo2DViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 04/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import SDWebImage

class Photo2DViewController: UIViewController {

    // MARK: Properties
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var localizer = LocalizationManager.shared
    
    var images: [String] = []
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = localizer[Buttons.photo].uppercased()
        
        pageControl.numberOfPages = images.count

    }

    // MARK: Apperance
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let offset = self.collectionView?.contentOffset;
        let width  = self.collectionView?.bounds.size.width;
        
        let index     = round(offset!.x / width!);
        let newOffset = CGPoint(x: index * size.width, y: offset!.y)
        
        self.collectionView?.setContentOffset(newOffset, animated: false)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.collectionView?.reloadData()
            self.collectionView?.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    
}

extension Photo2DViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Photo2DCollectionViewCell
        
        let pictureUrl = URL(string: images[indexPath.item])
        let placeholder = UIImage(named: "Slika 1")
        cell.cellImageView.sd_setImage(with: pictureUrl, placeholderImage: placeholder)
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
}

extension Photo2DViewController: UIScrollViewDelegate {
    // MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collectionView.contentOffset.x
        let w = self.collectionView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        
        pageControl.currentPage = currentPage
    }
}
