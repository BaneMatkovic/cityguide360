//
//  ListViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 12/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ListViewController: UIViewController, PutDataBack {

    // MARK: Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    var listArray: [List] = []
    
    var pomPanoramaId: String = ""
    
    var heartArray: [String] = []
    
    var internalState = ""
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = .white
        
        populateTable { (count) in
            print("count:", count)
            self.tableView.reloadData()
        }
        
        

    }
    
    // MARK: Actions
    
    func updateStateWith(_ newState: String) {
        self.internalState = newState
    }
    
    func dataBackToVC(text: String) {
        print("prosao kroz dataBackToVC")
        print(text)
       
    }
    
    // MARK: API
    
    func populateTable(completion: @escaping (Int) -> Void) {
        
        print("category in list view:", category)
        var url = ""
        if category == "sve" {
            url = "https://api.cityguide360.ba:1337/panorama/sve"
        } else {
            url = "https://api.cityguide360.ba:1337/panorama/category/\(category)"
        }
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                let listJSON: JSON = JSON(response.result.value!)
                
                var i = 0
                for _ in listJSON {
                    let name = listJSON[i]["name"].stringValue
                    let thumbnail = listJSON[i]["thumbnail"].stringValue
                    let thumbnailReady = "https://imagery.cityguide360.ba/\(thumbnail)"
                    let panoramaId = listJSON[i]["id"].stringValue
                    
                    let lm = List(name: name, thumbnail: thumbnailReady, panoramaId: panoramaId)
                    
                    self.listArray.append(lm)
                    
                    self.heartArray.append("White")

                    i += 1
                }
                completion(i)
            } else {
                print(response.result.error as Any)
            }
        }
        
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailFromList" {
            let destinationVc = segue.destination as! DetailWebViewController
            destinationVc.panoramaId = pomPanoramaId
        }
    }
    
}

// MARK: Tableview delegate & datasource

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListViewCell
        
        DispatchQueue.main.async {
            cell.listModel = self.listArray[indexPath.row]
        }
        
        if heartArray[indexPath.row] == "White"{
            print("kroz white")
            //cell.actionButtonInfo.tintColor = .white
            cell.actionButtonInfo.buttonImageColor = .white
        }
        else {
            print("kroz red")
            cell.actionButtonInfo.buttonImageColor = .red
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pomPanoramaId = listArray[indexPath.row].panoramaId
        heartArray[indexPath.row] = "Red"
        performSegue(withIdentifier: "toDetailFromList", sender: self)
    }

    
}
