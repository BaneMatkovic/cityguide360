//
//  MapCategoryViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 23/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import iOSDropDown

class MapCategoryViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var palleteView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var containerA: UIView!
    @IBOutlet weak var segmentedView: UISegmentedControl!
    @IBOutlet weak var containerB: UIView!
    
    
    var localizer = LocalizationManager.shared
    
    let searchTextField = SearchTextField()
    
    let dropDown = DropDown()
    
    let reachability = Reachability()!
    
    fileprivate func showSideMenu() {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        //self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }
    
    fileprivate func addPalleteView() {
        let menuImage = UIImage(named: "back2")?.withRenderingMode(.alwaysTemplate)
        let searchImage = UIImage(named: "searchFancy")?.withRenderingMode(.alwaysTemplate)
        menuButton.setImage(menuImage, for: .normal)
        searchButton.setImage(searchImage, for: .normal)
        menuButton.tintColor = Theme.Colors.secondaryTextColor
        searchButton.tintColor = Theme.Colors.secondaryTextColor
        
        palleteView.layer.cornerRadius = 2
        palleteView.layer.masksToBounds = true
        
        /*
        palleteView.addSubview(searchTextField)
        searchTextField.delegate = self
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.leftAnchor.constraint(equalTo: palleteView.leftAnchor, constant: Theme.Dimensions.padding).isActive = true
        searchTextField.rightAnchor.constraint(equalTo: searchButton.leftAnchor, constant: -20).isActive = true
        searchTextField.centerYAnchor.constraint(equalTo: palleteView.centerYAnchor).isActive = true
        searchTextField.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        */
        //palleteView.addSubview(dropDown)
        view.addSubview(dropDown)
        dropDown.translatesAutoresizingMaskIntoConstraints = false
        dropDown.leftAnchor.constraint(equalTo: palleteView.leftAnchor, constant: Theme.Dimensions.padding + 30).isActive = true
        dropDown.rightAnchor.constraint(equalTo: searchButton.rightAnchor, constant: -40).isActive = true
        dropDown.centerYAnchor.constraint(equalTo: palleteView.centerYAnchor).isActive = true
        dropDown.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        dropDown.textAlignment = .center
        dropDown.placeholder = localizer[Placeholders.clickToChooseCity]
        dropDown.arrowSize = 0
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        didBecomeActiveCheck()
        
        addPalleteView()
        
        //showSideMenu()
        
        print("Category from home::\(category)")
        
        dropDown.isUserInteractionEnabled = false

        
        segmentedView.setTitle(localizer[Segment.map], forSegmentAt: 0)
        segmentedView.setTitle(localizer[Segment.listView], forSegmentAt: 1)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setReachabilityNotifier()
        view.bringSubviewToFront(dropDown)
        
        let defaults = UserDefaults.standard
        let city = defaults.string(forKey: "city")
        
        if city != "" {
            
            switch category {
            case "995ef76ef2038fa803230001":
                dropDown.text = localizer[Labels.aerial]
            case "995ef76ef2038fa803230002":
                dropDown.text = localizer[Labels.foodAndDrink]
            case "995ef76ef2038fa803230003":
                dropDown.text = localizer[Labels.realEstates]
            case "995ef76ef2038fa803230005":
                dropDown.text = localizer[Labels.transport]
            case "995ef76ef2038fa803230004":
                dropDown.text = localizer[Labels.shopping]
            case "995ef76ef2038fa803230006":
                dropDown.text = localizer[Labels.sportAndRecreation]
            case "995ef76ef2038fa803230007":
                dropDown.text = localizer[Labels.services]
            case "995ef76ef2038fa803230008":
                dropDown.text = localizer[Labels.hotel]
            case "995ef76ef2038fa803230010":
                dropDown.text = localizer[Labels.nightLife]
            case "995ef76ef2038fa803230011":
                dropDown.text = localizer[Labels.artAndCulture]
            case "995ef76ef2038fa803230009":
                dropDown.text = localizer[Labels.tourismAndAttractions]
            case "995ef76ef2038fa803230012":
                dropDown.text = localizer[Labels.general]
            default:
                dropDown.text = localizer[Labels.allCategories]
            }
        }
    }

    //MARK: - Utilities
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    // MARK: - Actions
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        
        //view.bringSubviewToFront(view)
        //view.layoutIfNeeded()
        //dropDown.becomeFirstResponder()
        print("search button tapped")
        performSegue(withIdentifier: "fromMapCategory", sender: self)
     
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.containerA.alpha = 1
                self.containerB.alpha = 0
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.containerA.alpha = 0
                self.containerB.alpha = 1
            })
        }
        
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromMapCategory" {
            //let destinationVC = segue.destination as! SearchViewController
            
        }
    }
    
}

extension MapCategoryViewController: UITextFieldDelegate {
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let text = textField.text {
            print(text)
            
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchTextField.text = ""
        return true
    }
    
    fileprivate func searcTextFieldBottomBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: self.searchTextField.frame.size.height - width, width: self.searchTextField.frame.size.width, height: self.searchTextField.frame.size.height)
        border.borderWidth = width
        self.searchTextField.layer.addSublayer(border)
        self.searchTextField.layer.masksToBounds = true
        self.searchTextField.placeholder = localizer[Placeholders.search]
    }
    
    
  
}

extension MapCategoryViewController: InternetObserver {
    // MARK: - Internet Observer
    func setReachabilityNotifier() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("could not start notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        switch reachability.connection {
        case .wifi:
            print("via Wifi")
        case .cellular:
            print("via cellua")
        case .none:
            print("Network not reachable")
            self.alert(message: "Check Internet connection!", title: "Warning")
        }
    }
    
    func didBecomeActiveCheck() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged(note:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }
    
}
