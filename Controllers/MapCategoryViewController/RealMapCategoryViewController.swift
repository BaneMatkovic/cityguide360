//
//  RealMapCategoryViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 12/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

class RealMapCategoryViewController: UIViewController, GMSMapViewDelegate {

    // MARK: - Properties
    var mapView = GMSMapView()
    
    let infoMarker = GMSMarker()
    
    var hotSpot: [HotSpot] = []
    var pomPanoramaId = ""
    
    fileprivate func displayMap(lat: Double, long: Double) {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12.0)
        infoMarker.position.latitude = 44.538040
        infoMarker.position.longitude = 18.672180
        
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.mapView.delegate = self
        self.mapView.mapType = .normal
        //mapView.mapType = .satellite
        view = self.mapView
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        let city = defaults.string(forKey: "city") ?? ""
        let contain = cities.contains(city)
            if contain {
                switch city {
                case "TUZLA":
                    displayMap(lat: tuzlaCity.lat, long: tuzlaCity.long)
                default:
                    displayMap(lat: tuzlaCity.lat, long: tuzlaCity.long)
                }
            } else {
                displayMap(lat: tuzlaCity.lat, long: tuzlaCity.long)
            }
        
        getHotSpots(category: category) { (number) in
            let new = self.hotSpot.filterDuplicate{ ($0.lat, $0.long) }
            for hot in new {
                let mark = GMSMarker()
                mark.position = CLLocationCoordinate2D(latitude: hot.lat, longitude: hot.long)
                mark.title = hot.id
                mark.icon = UIImage(named: hot.category)
                mark.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
                mark.map = self.mapView
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        mapView.selectedMarker = nil
    }
    
    // MARK: - API calls
    
    func getHotSpots(category: String, completion: @escaping (Int) -> Void) {
        
        print("category in real:", category)
        var url = ""
        if category == "sve" {
            url = "https://api.cityguide360.ba:1337/panorama/map"
        } else {
            url = "https://api.cityguide360.ba:1337/panorama/category/\(category)"
        }
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                let hotSpotJSON: JSON = JSON(response.result.value!)
                var j = 0
                var i = 0
                for _ in hotSpotJSON {
                    let lat = hotSpotJSON[i]["lat"].doubleValue
                    let lon = hotSpotJSON[i]["lon"].doubleValue
                    let id = hotSpotJSON[i]["id"].stringValue
                    let myCategory = hotSpotJSON[i]["category"].stringValue
                    print("myCategorie:\(myCategory)")
                    let hs = HotSpot(lat: lat, long: lon, id: id, category: myCategory)
                    if lat != 0 {
                        self.hotSpot.append(hs)
                        j += 1
                    }
                    i += 1
                }
                completion(j)
            } else {
                print(response.result.error as Any)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String, name: String, location: CLLocationCoordinate2D) {
        print("You tapped \(name): \(placeID), \(location.latitude)/\(location.longitude)")
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        let view = UIView()

        pomPanoramaId = marker.title ?? ""
        self.performSegue(withIdentifier: "toDetailWebView", sender: self)
        
        return view
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailWebView" {
            let destinationVc = segue.destination as! DetailWebViewController
            destinationVc.panoramaId = pomPanoramaId
        }
    }



}
