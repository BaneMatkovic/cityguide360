//
//  InfoTableViewCell.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 03/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    @IBOutlet weak var descriptionLabelXL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
