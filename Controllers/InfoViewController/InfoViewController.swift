//
//  InfoViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 16/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import WebKit
import YoutubePlayer_in_WKWebView
import MessageUI
import JJFloatingActionButton
import CoreData

var nearByPanoramaId = ""
class InfoViewController: UIViewController {
    
    // MARK: Properties
  
  
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var adressImage: UIImageView!
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var mailImage: UIImageView!
    @IBOutlet weak var webImage: UIImageView!
    @IBOutlet weak var workTimeImage: UIImageView!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var adressStackView: UIStackView!
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var mailStackView: UIStackView!
    @IBOutlet weak var webStackView: UIStackView!
    @IBOutlet weak var workTimeStackView: UIStackView!
    @IBOutlet weak var photo360Button: UIButton!
    @IBOutlet weak var video360Button: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var nearByButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var panoramaId: String = ""
    var sound = false
    @IBOutlet weak var soundButton: UIBarButtonItem!
    let defaults = UserDefaults.standard
    var localizer = LocalizationManager.shared
    let actionButtonInfo = JJFloatingActionButton()
    var favoritesList: [Favorites] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let panorama360Photo: [Panorama360Photo] = []
    
    var objDescription = NSMutableArray()
    var webAction = 0
    var mailAction = 0
    var feedBackEmail = ""
    var phoneNumber = ""
    var link = ""
    var latitude: Double = 0
    var longitude: Double = 0
    var group = ""
    
    var images2D: [String] = []
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
        print("Info panoramaId:\(panoramaId)")
        nearByPanoramaId = panoramaId
        loadPanoramaInfo(panoramaId: panoramaId)
        
        localize()
        
        tableView.estimatedRowHeight = 34
        tableView.rowHeight = UITableView.automaticDimension
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleWebTap(sender:)))
        webStackView.addGestureRecognizer(tap)
        
        let tapMail = UITapGestureRecognizer(target: self, action: #selector(self.handleMailTap(sender:)))
        mailStackView.addGestureRecognizer(tapMail)
        
        let tapPhone = UITapGestureRecognizer(target: self, action: #selector(self.handlePhoneTap(sender:)))
        phoneStackView.addGestureRecognizer(tapPhone)
        
        let backImage = UIImage(named: "back2")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.backItem?.title = ""
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        
   
    }
    
    
    
    // MARK: - Apperance
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func localize() {
        let locale = defaults.string(forKey: "locale")
        if let language = Language(rawValue: locale ?? "English") {
            localizer.changeLanguage(language)
        }
        
        photo360Button.setTitle(localizer[Buttons.photo360].uppercased(), for: .normal)
        video360Button.setTitle(localizer[Buttons.video360].uppercased(), for: .normal)
        photoButton.setTitle(localizer[Buttons.photo].uppercased(), for: .normal)
        navigationButton.setTitle(localizer[Buttons.navigation].uppercased(), for: .normal)
        nearByButton.setTitle(localizer[Buttons.nearBy].uppercased(), for: .normal)
        shareButton.setTitle(localizer[Buttons.share].uppercased(), for: .normal)
        
        photo360Button.layer.borderWidth = 1
        photo360Button.layer.borderColor = UIColor.white.cgColor
        
        video360Button.layer.borderWidth = 1
        video360Button.layer.borderColor = UIColor.white.cgColor
        
        photoButton.layer.borderWidth = 1
        photoButton.layer.borderColor = UIColor.white.cgColor
        
        navigationButton.layer.borderWidth = 1
        navigationButton.layer.borderColor = UIColor.white.cgColor
        
        nearByButton.layer.borderWidth = 1
        nearByButton.layer.borderColor = UIColor.white.cgColor
        
        shareButton.layer.borderWidth = 1
        shareButton.layer.borderColor = UIColor.white.cgColor
    }
    
    
    // MARK: API
    
    func loadPanoramaInfo(panoramaId: String) {
        
       let url = "https://api.cityguide360.ba:1337/panorama/\(panoramaId)"
        print("REQUEST:", url as Any)
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                let panJSON: JSON = JSON(response.result.value!)
                
                let name = panJSON["name"].stringValue
                self.nameLabel.text = name
                
                let category = panJSON["category"].stringValue
                switch category {
                case "995ef76ef2038fa803230001":
                    self.categoryLabel.text = self.localizer[Labels.aerial]
                case "995ef76ef2038fa803230002":
                    self.categoryLabel.text = self.localizer[Labels.foodAndDrink]
                case "995ef76ef2038fa803230003":
                    self.categoryLabel.text = self.localizer[Labels.realEstates]
                case "995ef76ef2038fa803230005":
                    self.categoryLabel.text = self.localizer[Labels.transport]
                case "995ef76ef2038fa803230004":
                    self.categoryLabel.text = self.localizer[Labels.shopping]
                case "995ef76ef2038fa803230006":
                    self.categoryLabel.text = self.localizer[Labels.sportAndRecreation]
                case "995ef76ef2038fa803230007":
                    self.categoryLabel.text = self.localizer[Labels.services]
                case "995ef76ef2038fa803230008":
                    self.categoryLabel.text = self.localizer[Labels.hotel]
                case "995ef76ef2038fa803230010":
                    self.categoryLabel.text = self.localizer[Labels.nightLife]
                case "995ef76ef2038fa803230011":
                    self.categoryLabel.text = self.localizer[Labels.artAndCulture]
                case "995ef76ef2038fa803230009":
                    self.categoryLabel.text = self.localizer[Labels.tourismAndAttractions]
                case "995ef76ef2038fa803230012":
                    self.categoryLabel.text = self.localizer[Labels.general]
                default:
                    self.categoryLabel.text = self.localizer[Labels.general]
                }
                
                let address = panJSON["place"]["adresa"].stringValue
                if address == "" {
                    self.adressStackView.isHidden = false
                    self.adressLabel.text = self.localizer[Messages.noResults]
                    
                    let phone = panJSON["kontaktTelefon"].stringValue
                    if phone == "" {
                        self.phoneStackView.isHidden = false
                        self.phoneLabel.text = self.localizer[Messages.noResults]
                    } else {
                    self.phoneLabel.text = phone
                    self.phoneNumber = phone
                    }
                    
                    let mail = panJSON["kontaktEmail"].stringValue
                    if mail == "" {
                        self.mailStackView.isHidden = false
                        self.mailLabel.text = self.localizer[Messages.noResults]
                        self.mailAction = 0
                    } else {
                        self.mailLabel.text = mail
                        self.mailAction = 1
                        self.feedBackEmail = mail
                    }
                    
                    let web = panJSON["website"].stringValue
                    if web == "" {
                        self.webStackView.isHidden = false
                        self.webLabel.text = self.localizer[Messages.noResults]
                        self.webAction = 0
                    } else {
                        self.webLabel.text = web
                        self.webAction = 1
                        shareUrl = web
                    }
                    
                    let radnoVrijeme = panJSON["radnoVrijeme"].stringValue
                    if radnoVrijeme == "" {
                        self.workTimeStackView.isHidden = false
                        self.workTimeLabel.text = self.localizer[Messages.noResults]
                    } else {
                        let locale = self.defaults.string(forKey: "locale")
                        if locale == "Bosnian" {
                        self.workTimeLabel.text = radnoVrijeme.htmlToString
                        }
                    }
                    
                    let radnoVrijemeEn = panJSON["radnoVrijemeEn"].stringValue
                    if radnoVrijemeEn == "" {
                        self.workTimeStackView.isHidden = false
                        self.workTimeLabel.text = self.localizer[Messages.noResults]
                    } else {
                        let locale = self.defaults.string(forKey: "locale")
                        if locale == "English" {
                            self.workTimeLabel.text = radnoVrijemeEn.htmlToString
                        }
                    }
                    
                    let radnoVrijemeDe = panJSON["radnoVrijemeDe"].stringValue
                    if radnoVrijemeDe == "" {
                        self.workTimeStackView.isHidden = false
                        self.workTimeLabel.text = self.localizer[Messages.noResults]
                    } else {
                        let locale = self.defaults.string(forKey: "locale")
                        if locale == "Deutchland" {
                            self.workTimeLabel.text = radnoVrijemeDe.htmlToString
                        }
                    }
                    
                    let description = panJSON["description"].stringValue
                    if description == "" {
                        self.objDescription.add(self.localizer[Messages.noDetails].htmlToString)
                    } else {
                        let locale = self.defaults.string(forKey: "locale")
                        if locale == "Bosnian" {
                            self.objDescription.add(description.htmlToString)
                        }
                    }
                    
                    let descriptionEn = panJSON["descriptionEn"].stringValue
                    if descriptionEn == "" {
                        //self.objDescription.add(self.localizer[Messages.noDetails].htmlToString)
                    } else {
                        let locale = self.defaults.string(forKey: "locale")
                        if locale == "English" {
                            self.objDescription.add(descriptionEn.htmlToString)
                        }
                    }
                    
                    let descriptionDe = panJSON["descriptionDe"].stringValue
                    if descriptionDe == "" {
                        //self.objDescription.add(self.localizer[Messages.noDetails].htmlToString)
                    } else {
                        let locale = self.defaults.string(forKey: "locale")
                        if locale == "Deutchland" {
                            self.objDescription.add(descriptionDe.htmlToString)
                        }
                    }
                    
                    let lat = panJSON["lat"].doubleValue
                    self.latitude = lat
                    
                    let lon = panJSON["lon"].doubleValue
                    self.longitude = lon
                    
                    let grop = panJSON["group"].stringValue
                    self.group = grop
                    
                    if address == "" && phone == "" && mail == "" && mail == "" && web == "" && radnoVrijeme == "" && radnoVrijemeEn == "" && radnoVrijemeDe == "" && description == "" && descriptionEn == "" && descriptionDe == "" {
                        print("all empty")
                        //self.containerViewHeightConstraint.constant = 0
                    }
                    
                
                    
                } else {
                    print("else")
                    self.adressLabel.text = address
                    
                    let phone = panJSON["place"]["phone"].stringValue
                    self.phoneLabel.text = phone
                    self.phoneNumber = phone
                    
                    let mail = panJSON["place"]["email"].stringValue
                    self.mailLabel.text = mail
                    self.mailAction = 1
                    self.feedBackEmail = mail
                    
                    let web = panJSON["place"]["site"].stringValue
                    self.webLabel.text = web
                    self.webAction = 1
                    shareUrl = web
                    
                    let locale = self.defaults.string(forKey: "locale")
                    print("locale:", locale as Any)
                    let radnoVrijeme = panJSON["place"]["radnoVrijeme"].stringValue
                    if locale == "Bosnian" {
                        self.workTimeLabel.text = radnoVrijeme.htmlToString
                    }
                    
                    let radnoVrijemeEn = panJSON["place"]["radnoVrijemeEn"].stringValue
                    if locale == "English" {
                        self.workTimeLabel.text = radnoVrijemeEn.htmlToString
                    }
                    
                    let radnoVrijemeDe = panJSON["place"]["radnoVrijemeDe"].stringValue
                    if locale == "Deutchland" {
                        self.workTimeLabel.text = radnoVrijemeDe.htmlToString
                    }
                    
                    let description = panJSON["place"]["description"].stringValue
                    if locale == "Bosnian" {
                        self.objDescription.add(description.htmlToString)
                    }
                    
                    let descriptionEN = panJSON["place"]["descriptionEN"].stringValue
                    if locale == "English" {
                        self.objDescription.add(descriptionEN.htmlToString)
                    }
                    
                    let descriptionDE = panJSON["place"]["descriptionDE"].stringValue
                    if locale == "Deutchland" {
                        self.objDescription.add(descriptionDE.htmlToString)
                    }
                    
                    let youTubeLink = panJSON["place"]["youtubeLink"].stringValue
                    self.link = youTubeLink
                    
                }
                
                let thumbnail = panJSON["thumbnail"]
                let readyThumbnail = "http://imagery.cityguide360.ba/\(thumbnail)"
                let pictureUrl = URL(string: readyThumbnail)
                self.thumbImageView.sd_setImage(with: pictureUrl)
                

                var i = 0
                for _ in panJSON{
                let images = panJSON["place"]["images"][i]["imageFull"].stringValue
                    
                    if images != "" {
                        let readyImage = "http://imagery.cityguide360.ba/\(images)"
                        self.images2D.append(readyImage)
                    }
                    
                 i += 1
                }
                
                self.tableView.reloadData()
                
            } else {
                print(response.result.error as Any)
            }
        }
    }

    // MARK: Actions
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func soundButtonTapped(_ sender: Any) {
        sound = !sound
        
        if sound {
            soundButton.image = UIImage(named: "sound")
        } else {
            soundButton.image = UIImage(named: "mute")
        }
    }

    @IBAction func shareButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4) {
            self.shareButton.backgroundColor = UIColor(hexString: "#5384ed")
        }
        shareItems([localizer[Messages.shareApp], "\n", shareUrl], onViewController: self)
        UIView.animate(withDuration: 0.4) {
            self.shareButton.backgroundColor = UIColor(hexString: "#121925")
        }
        
    }
    
    @objc func handleWebTap(sender: UITapGestureRecognizer? = nil) {
        guard let url2=URL(string: shareUrl) else { return }
        UIApplication.shared.open(url2, options: [:], completionHandler: nil)
    }
    
    @objc func handleMailTap(sender: UITapGestureRecognizer? = nil) {
        if mailAction > 0 {
            if MFMailComposeViewController.canSendMail() {
                let mailComposerViewController = MFMailComposeViewController()
                mailComposerViewController.setToRecipients([feedBackEmail])
                mailComposerViewController.mailComposeDelegate = self
                present(mailComposerViewController, animated: true)
            }
        }
    }
    
    @objc func handlePhoneTap(sender: UITapGestureRecognizer? = nil) {
      
        if phoneNumber != "" {
        let newPhone = phoneNumber.replacingOccurrences(of:"+", with: "00")
        let newPhoneFinal = "tel://" + newPhone.replacingOccurrences(of:" ", with: "")
        UIApplication.shared.open(URL(string: newPhoneFinal)!, options: [:], completionHandler: nil)
        }
  
    }
    
    @IBAction func video360ButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.4) {
            self.video360Button.backgroundColor = UIColor(hexString: "#5384ed")
        }
        if self.link != "" {
        self.performSegue(withIdentifier: "showVideo", sender: self)
        } else {
            self.alert(message: "No video")
        }
        UIView.animate(withDuration: 0.4) {
            self.video360Button.backgroundColor = UIColor(hexString: "#121925")
        }
    }
    
    @IBAction func photoButtonTaped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4) {
            self.photoButton.backgroundColor = UIColor(hexString: "#5384ed")
        }
        if self.images2D.count > 0 {
            self.performSegue(withIdentifier: "showPhoto2D", sender: self)
        } else {
            self.alert(message: "No photo")
        }
        UIView.animate(withDuration: 0.4) {
            self.photoButton.backgroundColor = UIColor(hexString: "#121925")
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: UIButton) {
        
            UIView.animate(withDuration: 0.4) {
                self.navigationButton.backgroundColor = UIColor(hexString: "#5384ed")
            }
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                if let url2 = URL(string: "comgooglemaps://?saddr=&daddr=\(Float(self.latitude)),\(Float(self.longitude))&directionsmode=driving") {
            //UIApplication.shared.openURL(NSURL(string:"comgooglemaps://?saddr=&daddr=\(Float(self.latitude)),\(Float(self.longitude))&directionsmode=driving")! as URL)
                UIApplication.shared.open(url2, options: [:], completionHandler: nil)
                }
            } else {
                NSLog("Can't use com.google.maps://");
            }
        
        UIView.animate(withDuration: 0.4) {
            self.navigationButton.backgroundColor = UIColor(hexString: "#121925")
        }

    }
    
    @IBAction func photo360ButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4) {
            self.photo360Button.backgroundColor = UIColor(hexString: "#5384ed")
        }
        
        if self.group != "" {
        self.performSegue(withIdentifier: "showPhoto360", sender: self)
        } else {
            self.alert(message: "No 360photo")
        }
        
        UIView.animate(withDuration: 0.4) {
            self.photo360Button.backgroundColor = UIColor(hexString: "#121925")
        }
    }
    
    @IBAction func nearByButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4) {
            self.nearByButton.backgroundColor = UIColor(hexString: "#5384ed")
        }
        
     
        self.performSegue(withIdentifier: "showNearBy", sender: self)
        
        
        UIView.animate(withDuration: 0.4) {
            self.nearByButton.backgroundColor = UIColor(hexString: "#121925")
        }
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVideo" {
            let destinationVC = segue.destination as! Video360ViewController
            destinationVC.link = self.link
        }
        
        if segue.identifier == "showPhoto2D" {
            let destinationVC = segue.destination as! Photo2DViewController
            destinationVC.images = self.images2D
        }
        
        if segue.identifier == "showNavigation" {
            let destinationVC = segue.destination as! NavigationViewController
            destinationVC.targetLat = latitude
            destinationVC.targetLon = longitude
        }
        
        if segue.identifier == "showPhoto360" {
            let destinationVC = segue.destination as! Photo360ViewController
            destinationVC.group = self.group
            destinationVC.latitude = self.latitude
            destinationVC.longitude = self.longitude
        }
        
        if segue.identifier == "showNearBy" {
            let destinationVC = segue.destination as! NearByContainerViewController
            destinationVC.latitude = self.latitude
            destinationVC.longitude = self.longitude
            nearByLatitude = self.latitude
            nearByLongitude = self.longitude
        }
    }

}

// MARK: - Table view data source

extension InfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objDescription.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! InfoTableViewCell
        
        cell.selectionStyle = .none
        cell.descriptionLabelXL.textColor = .white
        cell.descriptionLabelXL.text = self.objDescription[indexPath.row] as? String

        return cell
    }
    
}

extension InfoViewController: MFMailComposeViewControllerDelegate {
    
    // MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}


