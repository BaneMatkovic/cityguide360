//
//  NavigationViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 04/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class NavigationViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    // MARK: Properties
    @IBOutlet weak var mapView: GMSMapView!
    
    let locationManager = CLLocationManager()
    let infoMarker = GMSMarker()
    
    var sourceLat: Double = 0
    var sourceLon: Double = 0
    var targetLat: Double = 0
    var targetLon: Double = 0
    
    var a: CLLocationCoordinate2D!
    var b: CLLocationCoordinate2D!
    //Your map initiation code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
 
        print("targetLat:", targetLat)
        print("targeetLon:", targetLon)
        
        b = CLLocationCoordinate2D(latitude: targetLat, longitude: targetLon)
        
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        openGoogleMapAppp()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            
            print("longitute = \(location.coordinate.longitude), latitude = \(location.coordinate.latitude)")
            sourceLon = location.coordinate.longitude
            sourceLat = location.coordinate.latitude
            a = CLLocationCoordinate2D(latitude: sourceLat, longitude: sourceLon)
            
            let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 17.0)
            infoMarker.position.latitude = location.coordinate.latitude
            infoMarker.position.longitude = location.coordinate.longitude
            infoMarker.map = mapView
            self.mapView?.animate(to: camera)
            
            //fetchRoute(from: a, to: b)
            openGoogleMapAppp()
        }
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    // MARK: Navigation Drive
    // IMPORTANT: In Developer Google Console must enable: GeoCoding Api, GeoLaction Api, Directions Api, Maps JavaScript  Api
    
    /*
    func fetchRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
        
        let session = URLSession.shared
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLat),\(sourceLon)&destination=\(targetLat),\(targetLon)&sensor=false&mode=driving&key=AIzaSyB_cZLFxUgHwFS2CWuwgfAUBNiKuD0kLj8")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            guard let jsonResult = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any], let jsonResponse = jsonResult else {
                print("error in JSONSerialization")
                return
            }
            //print("jsonResult:", jsonResult as Any)
            
            guard let routes = jsonResponse["routes"] as? [Any] else {
                return
            }
            
            guard let overview_polyline = routes[0] as? [String: Any] else {
                return
            }
            
            guard let polyLineString = overview_polyline["points"] as? String else {
                return
            }
            
            //Call this method to draw path on map
            self.drawPath(from: polyLineString)
        })
        task.resume()
    }
    
    func drawPath(from polyStr: String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = mapView // Google MapView
    }
    */
    func openGoogleMapAppp() {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(Float(targetLat)),\(Float(targetLon))&directionsmode=driving")! as URL)
        } else
        {
            NSLog("Can't use com.google.maps://");
        }
    }

}


