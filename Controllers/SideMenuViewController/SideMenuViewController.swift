//
//  SideMenuViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 19/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import MessageUI

class SideMenuViewController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var feedbackButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var favouritesButton: UIButton!
    @IBOutlet weak var bosanskiButton: SpinnerButton!
    @IBOutlet weak var englishButton: SpinnerButton!
    @IBOutlet weak var deutchlandButton: SpinnerButton!
    
    
    var localizer = LocalizationManager.shared
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeButton.leftImage(image: UIImage(named: "icon_home")!, renderMode: .alwaysTemplate)
        homeButton.tintColor = .white
        homeButton.setTitle(localizer[Buttons.home], for: .normal)
        
        contactButton.leftImage(image: UIImage(named: "contact")!, renderMode: .alwaysTemplate)
        contactButton.tintColor = .white
        contactButton.setTitle(localizer[Buttons.contactUs], for: .normal)
        
        feedbackButton.leftImage(image: UIImage(named: "email2")!, renderMode: .alwaysTemplate)
        feedbackButton.tintColor = .white
        feedbackButton.setTitle(localizer[Buttons.sendFeedbback], for: .normal)
        
        helpButton.leftImage(image: UIImage(named: "help")!, renderMode: .alwaysTemplate)
        helpButton.tintColor = .white
        helpButton.setTitle(localizer[Buttons.help], for: .normal)
        
        favouritesButton.leftImage(image: UIImage(named: "heart")!, renderMode: .alwaysTemplate)
        favouritesButton.tintColor = .white
        favouritesButton.setTitle(localizer[Buttons.myFavourites], for: .normal)
        
        bosanskiButton.leftImage(image: UIImage(named: "ba_flag")!, renderMode: .alwaysOriginal)
        bosanskiButton.backgroundColor = Theme.Colors.google
        bosanskiButton.setTitle("               BIH ", for: .normal)
        bosanskiButton.contentHorizontalAlignment = .center
        bosanskiButton.addTarget(self, action: #selector(bosnianButtonTapped), for: .touchUpInside)
        
        englishButton.leftImage(image: UIImage(named: "en_flag")!, renderMode: .alwaysOriginal)
        englishButton.backgroundColor = Theme.Colors.google
        englishButton.setTitle("               ENG ", for: .normal)
        englishButton.contentHorizontalAlignment = .center
        englishButton.addTarget(self, action: #selector(engButtonTapped), for: .touchUpInside)
        
        deutchlandButton.leftImage(image: UIImage(named: "de_flag")!, renderMode: .alwaysOriginal)
        deutchlandButton.backgroundColor = Theme.Colors.google
        deutchlandButton.setTitle("               DEU ", for: .normal)
        deutchlandButton.contentHorizontalAlignment = .center
        deutchlandButton.addTarget(self, action: #selector(deutchButtonTapped), for: .touchUpInside)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.revealViewController().frontViewController.view.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
    self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.revealViewController().frontViewController.revealViewController().tapGestureRecognizer()
        self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
    }
    
    // MARK: - Apperance
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func localize() {
        homeButton.setTitle(localizer[Buttons.home], for: .normal)
        contactButton.setTitle(localizer[Buttons.contactUs], for: .normal)
        feedbackButton.setTitle(localizer[Buttons.sendFeedbback], for: .normal)
        helpButton.setTitle(localizer[Buttons.help], for: .normal)
        favouritesButton.setTitle(localizer[Buttons.myFavourites], for: .normal)
    }

    
    // MARK: - Actions
    
    @IBAction func homeButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func helpButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func contactButtonTapped(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "tel://0038755379221")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func feedbackButtonTapped(_ sender: UIButton) {
        
        if MFMailComposeViewController.canSendMail() {
            let mailComposerViewController = MFMailComposeViewController()
            mailComposerViewController.setToRecipients([feedbackEmail])
            mailComposerViewController.mailComposeDelegate = self
            present(mailComposerViewController, animated: true)
        }
    }
    
    @IBAction func favouritesButtonTapped(_ sender: UIButton) {
        
    }
    
    @objc func bosnianButtonTapped() {
        
        bosanskiButton.startAnimating()
        
        if let language = Language(rawValue: "Bosnian") {
            localizer.changeLanguage(language)
            let defaults = UserDefaults.standard
            defaults.set("Bosnian", forKey: "locale")
        }
        
        self.localize()
        bosanskiButton.stopAnimating()
        bosanskiButton.leftImage(image: UIImage(named: "ba_flag")!, renderMode: .alwaysOriginal)
        bosanskiButton.setTitle("               BIH ", for: .normal)
        bosanskiButton.contentHorizontalAlignment = .center
    }
    
    @objc func engButtonTapped() {
        englishButton.startAnimating()
        
        if let language = Language(rawValue: "English") {
            localizer.changeLanguage(language)
            let defaults = UserDefaults.standard
            defaults.set("English", forKey: "locale")
        }
        
        self.localize()
        englishButton.stopAnimating()
        englishButton.leftImage(image: UIImage(named: "en_flag")!, renderMode: .alwaysOriginal)
        englishButton.setTitle("               ENG ", for: .normal)
        englishButton.contentHorizontalAlignment = .center
    }
    
    @objc func deutchButtonTapped() {
        deutchlandButton.startAnimating()
        
        if let language = Language(rawValue: "Deutchland") {
            localizer.changeLanguage(language)
            let defaults = UserDefaults.standard
            defaults.set("Deutchland", forKey: "locale")
        }
        
        self.localize()
        deutchlandButton.stopAnimating()
        deutchlandButton.leftImage(image: UIImage(named: "de_flag")!, renderMode: .alwaysOriginal)
        deutchlandButton.setTitle("               DEU ", for: .normal)
        deutchlandButton.contentHorizontalAlignment = .center
        
    }
    
}

extension SideMenuViewController: MFMailComposeViewControllerDelegate {
    
    // MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
