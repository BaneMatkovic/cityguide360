//
//  HomeWebViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 10/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import WebKit
import Alamofire
import SwiftyJSON

class HomeWebViewController: UIViewController, WKScriptMessageHandler {

    // MARK: - Properties
    
    var webView: WKWebView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var pomPanoramaId = "" //5bd9e581eba369b00f0255d7
    
    fileprivate let actionButton = JJFloatingActionButton()
    fileprivate let actionButtonInfo = JJFloatingActionButton()
    var localizer = LocalizationManager.shared
    var panoramaHome = ""
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupWebView()
        self.view.addSubview(self.webView)
        
        addActionButton()
        addInfoButton()
        localize()
        
        loadHomePanorama { (result) in
            self.panoramaHome = result
                if let url = URL(string: "https://ios.cityguide360.ba/panorama/\(self.panoramaHome)/search/ba/") {
                    let request = URLRequest(url: url)
                    self.webView.load(request)
                }
        }
        
        let preferredLanguage = NSLocale.preferredLanguages[0]
        print("iOS language:", preferredLanguage as Any)
    }
    
    // MARK: API
    
    func loadHomePanorama(completion: @escaping (String) -> Void) {
        let  url = "https://api.cityguide360.ba:1337/panorama/home"
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                let homePanoramaJSON: JSON = JSON(response.result.value!)
                let panId = homePanoramaJSON["id"].stringValue
                self.panoramaHome = panId
                self.pomPanoramaId = panId
                completion(self.panoramaHome)
            } else {
                print(response.result.error as Any)
            }
        }
    }
    
    // MARK: Apperance
    
    private func setupWebView() {
        
        let contentController = WKUserContentController()
        let userScript = WKUserScript(
            source: "mobileHeader()",
            injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.add(self, name: "loginAction")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: Localization
    fileprivate func localize() {
        let defaults = UserDefaults.standard
        let locale = defaults.string(forKey: "locale")
        if let language = Language(rawValue: locale ?? "English") {
            localizer.changeLanguage(language)
            if locale == nil {
                defaults.set("English", forKey: "locale")
            }
            
        }
    }
    
    // MARK: - Actions
    
    fileprivate func addActionButton() {
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonColor = UIColor(hexString: "#121925")
        actionButton.buttonImageColor = .white
        actionButton.addItem(title: "", image: UIImage(named: "icon_menu")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("action button clicked")
            self.performSegue(withIdentifier: "showReveal0", sender: self)
            
        }
        actionButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 50).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -52).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    fileprivate func addInfoButton() {
        view.addSubview(actionButtonInfo)
        actionButtonInfo.translatesAutoresizingMaskIntoConstraints = false
        actionButtonInfo.overlayView.backgroundColor = .clear
        actionButtonInfo.buttonColor = UIColor(hexString: "#121925")
        actionButtonInfo.buttonImageColor = .white
        actionButtonInfo.addItem(title: "", image: UIImage(named: "info")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("info button clicked")
            self.performSegue(withIdentifier: "showInfo", sender: self)
            
        }
        actionButtonInfo.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50).isActive = true
        actionButtonInfo.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -52).isActive = true
        actionButtonInfo.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReveal0" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal")
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "showInfo" {
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
            let navController = UINavigationController(rootViewController: vc)
            vc.panoramaId = pomPanoramaId
            self.present(navController, animated:true, completion: nil)
        }
    }
    
    // MARK: - WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "loginAction" {
            pomPanoramaId = message.body as! String
            print("JavaScript is sending a message \(message.body)")
        }
    }
 
}

extension WKWebView {
    override open var safeAreaInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
