//
//  HomePanoramaViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 26/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import Alamofire
import SwiftyJSON
//import SDWebImage
import GoogleMaps
import GVRKit

//, GMSMapViewDelegate, GMSPanoramaViewDelegate
class HomePanoramaViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var imageVRView: GVRPanoramaView!
    
    
    
    fileprivate let actionButton = JJFloatingActionButton()
    fileprivate let actionButtonInfo = JJFloatingActionButton()
    //var panorama = Panorama(sound: Sound.init(soundBS: "", soundEN: "", soundDE: ""))
    
    //let url = "http://api.cityguide360.ba:1337/panorama/home"
    
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    
    
    let markers = [(44.540210277762945, 18.665136111100608, "Slatina"),(44.536281, 18.670927, "Dzamija")]
    
    fileprivate func addActionButton() {
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonColor = UIColor(hexString: "#121925")
        actionButton.buttonImageColor = .white
        actionButton.addItem(title: "", image: UIImage(named: "icon_menu")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("action button clicked")
            self.performSegue(withIdentifier: "showReveal", sender: self)
            
        }
        actionButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 50).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -52).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    fileprivate func addInfoButton() {
        view.addSubview(actionButtonInfo)
        actionButtonInfo.translatesAutoresizingMaskIntoConstraints = false
        actionButtonInfo.overlayView.backgroundColor = .clear
        actionButtonInfo.buttonColor = UIColor(hexString: "#121925")
        actionButtonInfo.buttonImageColor = .white
        actionButtonInfo.addItem(title: "", image: UIImage(named: "info")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("info button clicked")
            
        }
        actionButtonInfo.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50).isActive = true
        actionButtonInfo.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -52).isActive = true
        actionButtonInfo.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    fileprivate func populatePanoramaView() {
    
        //print("panorama thumbnail:\(self.panorama.thumbnail)")
        //let url2 = URL(string: self.panorama.thumbnail )
        //print(url2 as Any)
        //let data2 = NSData(contentsOf: url2!)
        //self.imageVRView.load(UIImage(data: data2! as Data), of: GVRPanoramaImageType.mono)
        
        
        imageVRView.load(UIImage(named: "panoramaDefault"),
                         of: GVRPanoramaImageType.mono)
        
        imageVRView.enableCardboardButton = true
        imageVRView.enableFullscreenButton = false
        imageVRView.enableInfoButton = false
        imageVRView.delegate = self
        
        imageVRView.hidesTransitionView = false
        imageVRView.enableTouchTracking = true
        
        imageVRView.displayMode = .embedded

        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        addActionButton()
        addInfoButton()
        
        /*
        self.getPanaroma() { (response) in
            DispatchQueue.main.async (execute: { () -> Void in
            print("Response:", response)
            print("panorama thumbnail:\(self.panorama.thumbnail)")
            self.populatePanoramaView()
 
            })
        */

            //imageVRView.load(UIImage(named: "panoramaDefault"),
            // of: GVRPanoramaImageType.mono)
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.populatePanoramaView()
        
    }
    
    
    // MARK - Network
    /*
    func getPanoramaData(url: String, parameters: [String: String], completion: @escaping (String) -> Void) {
        
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                DispatchQueue.main.async {
                    let resultJSON: JSON = JSON(response.result.value!)
                    self.panorama.thumbnail = resultJSON["thumbnail"].stringValue
                    let imageStart = "http://imagery.cityguide360.ba/"
                    let imageFinal = imageStart + self.panorama.thumbnail
                    self.panorama.thumbnail = imageFinal
                    print("Final:\(self.panorama.thumbnail)")
                    completion(self.panorama.thumbnail)
        
                }
            } else {
                print("ERROR:",response.result.error as Any)
                //self.cityLabel.text = "Connection Issues"
            }
        }
      
    }
    
    func getPanaroma(completion: @escaping (String) -> Void) {
        let jsonUrlString = self.url
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
            guard let data = data else { return }
            
            do{
                let pan = try JSONDecoder().decode(Panorama.self, from: data)
                print("pan thumb------:", pan.thumbnail)
                self.panorama.thumbnail = pan.thumbnail
                let imageStart = "http://imagery.cityguide360.ba/"
                let imageFinal = imageStart + self.panorama.thumbnail
                self.panorama.thumbnail = imageFinal
                completion(self.panorama.thumbnail)
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
            }
            }.resume()
        
    }
    
    */
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReveal" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal")
            self.present(controller, animated: true, completion: nil)
        }
    }
    

}

extension HomePanoramaViewController: GVRWidgetViewDelegate {
    
    
    
    
}
