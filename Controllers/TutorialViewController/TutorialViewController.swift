//
//  TutorialViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 19/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var localizer = LocalizationManager.shared
    
    //let defaults = UserDefaults.standard
    //lazy var locale = self.defaults.string(forKey: "locale")
    
    
    var tutorials = [
        Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 V")!, color: UIColor(hexString: "#121925")),
        Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 V")!, color: UIColor(hexString: "#121925")),
        Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 V")!, color: UIColor(hexString: "#121925")),
        Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 V")!, color: UIColor(hexString: "#121925"))
        
    ]
    //UIColor(hexString: "#1DAEEB")
    // MARK: View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.setImage(UIImage(named: "failure")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = .white
        
        doneButton.layer.masksToBounds = false
        doneButton.setImage(UIImage(named: "success"), for: .normal)
        doneButton.tintColor = .white
        doneButton.layer.cornerRadius = 60 / 2
 
        pageControl.numberOfPages = tutorials.count
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let defaults = UserDefaults.standard
        let locale = defaults.string(forKey: "locale")
        
        if locale == "Deutchland" {
            tutorials = [
                Tutorial(title: "", description: "", image: UIImage(named: "DEU 1 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "DEU 2 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "DEU 3 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "DEU 4 V")!, color: UIColor(hexString: "#121925"))
            ]
        }
        
        if locale == "English" {
            tutorials = [
                Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 V")!, color: UIColor(hexString: "#121925"))
            ]
        }
        
        if locale == "Bosnian" {
            tutorials = [
                Tutorial(title: "", description: "", image: UIImage(named: "BIH 1 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "BIH 2 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "BIH 3 V")!, color: UIColor(hexString: "#121925")),
                Tutorial(title: "", description: "", image: UIImage(named: "BIH 4 V")!, color: UIColor(hexString: "#121925"))
            ]
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        let defaults = UserDefaults.standard
        let locale = defaults.string(forKey: "locale")
        print("locale:\(locale ?? "")")
        
        if locale == "Deutchland" {
            switch UIDevice.current.orientation {
            case .portrait:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .portraitUpsideDown:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .unknown:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .landscapeLeft:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 1 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 2 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 3 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 4 H")!, color: UIColor(hexString: "#121925"))
                ]
            case .landscapeRight:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 1 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 2 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 3 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "DEU 4 H")!, color: UIColor(hexString: "#121925"))
                ]
            case .faceUp:
                print("Face Up")
            case .faceDown:
                print("Face down")
            }
        }
        
        if locale == "English" {
            switch UIDevice.current.orientation {
            case .portrait:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .portraitUpsideDown:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .unknown:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .landscapeLeft:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 H")!, color: UIColor(hexString: "#121925"))
                ]
            case .landscapeRight:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 1 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 2 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 3 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "ENG 4 H")!, color: UIColor(hexString: "#121925"))
                ]
            case .faceUp:
                print("Face Up")
            case .faceDown:
                print("Face down")
            }
        }
        
        if locale == "Bosnian" {
            switch UIDevice.current.orientation {
            case .portrait:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .portraitUpsideDown:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .unknown:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 1 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 2 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 3 V")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 4 V")!, color: UIColor(hexString: "#121925"))
                ]
            case .landscapeLeft:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 1 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 2 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 3 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 4 H")!, color: UIColor(hexString: "#121925"))
                ]
            case .landscapeRight:
                tutorials = [
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 1 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 2 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 3 H")!, color: UIColor(hexString: "#121925")),
                    Tutorial(title: "", description: "", image: UIImage(named: "BIH 4 H")!, color: UIColor(hexString: "#121925"))
                ]
            case .faceUp:
                print("Face Up")
            case .faceDown:
                print("Face down")
            }
        }
        
        self.collectionView.reloadData()
    }
    
    // MARK: - Actions
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension TutorialViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutorials.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TutorialCollectionViewCell
        
        cell.tutorial = tutorials[indexPath.item]
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
}

extension TutorialViewController: UIScrollViewDelegate {
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collectionView.contentOffset.x
        let w = self.collectionView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        
        pageControl.currentPage = currentPage
        
        UIView.animate(withDuration: 0.3) {
            self.closeButton.alpha = currentPage == self.tutorials.count - 1 ? 1.0 : 0.0;
            self.doneButton.alpha = currentPage == self.tutorials.count - 1 ? 1.0 : 0.0;
        }
        print("scrollViewDidEndDecelerating")
    }
    
}
