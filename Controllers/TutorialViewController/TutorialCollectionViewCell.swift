//
//  TutorialCollectionViewCell.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 22/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var localizer = LocalizationManager.shared
    
    var tutorial: Tutorial! {
        didSet {
            imageView.image = tutorial.image
            //titleLabel.text = tutorial.title
            //descriptionLabel.text = tutorial.description
            containerView.backgroundColor = tutorial.color
            
        }
    }
    
}
