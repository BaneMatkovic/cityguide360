//
//  SearchTableViewCell.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 31/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import SDWebImage

class SearchTableViewCell: UITableViewCell {
   
    // MARK: - Properties
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cellIconImageView: UIImageView!
    
    var imageName = ""
    
    var searchModel: Search! {
        didSet {
            let pictureUrl = URL(string: searchModel.thumbnail)
            let placeholder = UIImage(named: "Slika 1")
            cellImageView.sd_setImage(with: pictureUrl, placeholderImage: placeholder)
            nameLabel.text = searchModel.name
            switch searchModel.categoryName {
            case "995ef76ef2038fa803230001":
                imageName = "zracna"
            case "995ef76ef2038fa803230002":
                imageName = "hrana-1"
            case "995ef76ef2038fa803230003":
                imageName = "nekretnine"
            case "995ef76ef2038fa803230005":
                imageName = "transport"
            case "995ef76ef2038fa803230004":
                imageName = "kupovina"
            case "995ef76ef2038fa803230006":
                imageName = "sport"
            case "995ef76ef2038fa803230007":
                imageName = "usluge"
            case "995ef76ef2038fa803230008":
                imageName = "smjestaj"
            case "995ef76ef2038fa803230010":
                imageName = "nocni_"
            case "995ef76ef2038fa803230011":
                imageName = "kultura"
            case "995ef76ef2038fa803230009":
                imageName = "turizam"
            case "995ef76ef2038fa803230012":
                imageName = "generalno"
            default:
                imageName = "generalno"
            }
            cellIconImageView.image = UIImage(named: imageName)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
