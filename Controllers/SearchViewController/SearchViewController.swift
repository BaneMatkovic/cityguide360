//
//  SearchViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 29/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SearchViewController: UIViewController {

    // MARK: Properties
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label1a: UILabel!
    @IBOutlet weak var aerialStackView: UIStackView!
    
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label2a: UILabel!
    @IBOutlet weak var servicesStackView: UIStackView!
    
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label3a: UILabel!
    @IBOutlet weak var foodAndDrinkStackView: UIStackView!
    
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label4a: UILabel!
    @IBOutlet weak var hotelStackView: UIStackView!
    
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label5a: UILabel!
    @IBOutlet weak var realEstatesStackVIew: UIStackView!
    
    @IBOutlet weak var label6: UILabel!
    @IBOutlet weak var label6a: UILabel!
    @IBOutlet weak var nightLifeStackView: UIStackView!
    
    @IBOutlet weak var label7: UILabel!
    @IBOutlet weak var label7a: UILabel!
    @IBOutlet weak var transportStackView: UIStackView!
    
    @IBOutlet weak var label8: UILabel!
    @IBOutlet weak var label8a: UILabel!
    @IBOutlet weak var artStackView: UIStackView!
    
    @IBOutlet weak var label9: UILabel!
    @IBOutlet weak var label9a: UILabel!
    @IBOutlet weak var shoppingStackView: UIStackView!
    
    @IBOutlet weak var label10: UILabel!
    @IBOutlet weak var label10a: UILabel!
    @IBOutlet weak var tourisamStackView: UIStackView!
    
    @IBOutlet weak var label11: UILabel!
    @IBOutlet weak var label11a: UILabel!
    @IBOutlet weak var sportStackView: UIStackView!
    
    @IBOutlet weak var label12: UILabel!
    @IBOutlet weak var label12a: UILabel!
    @IBOutlet weak var generalStackView: UIStackView!
    
    @IBOutlet weak var label13: UILabel!
    @IBOutlet weak var label13a: UILabel!
    @IBOutlet weak var allStackView: UIStackView!
    
    var localizer = LocalizationManager.shared
    
    var aerialTapped = false
    var servicesTapped = false
    var foodAndDrinkTapped = false
    var hotelTapped = false
    var realEstatesTapped = false
    var nightLifeTapped = false
    var transportTapped = false
    var artTapped = false
    var shoppingTapped = false
    var tourisamTapped = false
    var sportTapped = false
    var generalTapped = false
    var allTapped = false
    
    var selectedCategories: [String] = []
    
    var settedAllCat = false
    
    var searchArray: [Search] = []
    
    var pomPanoramaId: String = ""
    
    //let containerTap = UITapGestureRecognizer(target: self, action: #selector(containerViewTapped(sender: )))
    
    // MARK: View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
        tapGestureSetup()
        
        loadSearchDataHome { (count) in
            print("count home:", count)
            self.tableView.reloadData()
        }
        
        allSelected()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        AppUtility.lockOrientation(.portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.all)
    }
    
    // MARK: Apperance
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configure() {
        label1.text = localizer[Labels.aerial]
        label2.text = localizer[Labels.services]
        label3.text = localizer[Labels.foodAndDrink]
        label4.text = localizer[Labels.hotel]
        label5.text = localizer[Labels.realEstates]
        label6.text = localizer[Labels.nightLife]
        label7.text = localizer[Labels.transport]
        label8.text = localizer[Labels.artAndCulture]
        label9.text = localizer[Labels.shopping]
        label10.text = localizer[Labels.tourismAndAttractions]
        label11.text = localizer[Labels.sportAndRecreation]
        label12.text = localizer[Labels.general]
        label13.text = localizer[Labels.allCategories]
        
        searchTextField.placeholder = localizer[Placeholders.search]
        
        let backView: UIView = {
           let view = UIView()
            view.backgroundColor = .yellow
            return view
        }()
        
        tableView.tableFooterView = backView
        
        
        searchTextField.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "Artboard")?.withRenderingMode(.alwaysTemplate)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tintColor = UIColor.lightGray
        searchTextField.leftView = imageView
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    // MARK: Actions
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textFieldTouchDown(_ sender: UITextField) {
        let containerTap = UITapGestureRecognizer(target: self, action: #selector(containerViewTapped(sender: )))
        containerView.addGestureRecognizer(containerTap)
    }
    
    
    fileprivate func tapGestureSetup() {
        
        //let containerTap = UITapGestureRecognizer(target: self, action: #selector(containerViewTapped(sender: )))
        //containerView.addGestureRecognizer(containerTap)
        
        let aerialTap = UITapGestureRecognizer(target: self, action: #selector(aerialViewTapped(sender: )))
        aerialStackView.addGestureRecognizer(aerialTap)
        
        let servicesTap = UITapGestureRecognizer(target: self, action: #selector(servicesViewTapped(sender: )))
        servicesStackView.addGestureRecognizer(servicesTap)
        
        let foodAndDrinkTap = UITapGestureRecognizer(target: self, action: #selector(foodAndDrinkViewTapped(sender: )))
        foodAndDrinkStackView.addGestureRecognizer(foodAndDrinkTap)
        
        let hotelTap = UITapGestureRecognizer(target: self, action: #selector(hotelViewTapped(sender: )))
        hotelStackView.addGestureRecognizer(hotelTap)
        
        let realEstatesTap = UITapGestureRecognizer(target: self, action: #selector(realEstatesViewTapped(sender: )))
        realEstatesStackVIew.addGestureRecognizer(realEstatesTap)
        
        let nightLifeTap = UITapGestureRecognizer(target: self, action: #selector(nightLifeViewTapped(sender: )))
        nightLifeStackView.addGestureRecognizer(nightLifeTap)
        
        let transportTap = UITapGestureRecognizer(target: self, action: #selector(transportViewTapped(sender: )))
        transportStackView.addGestureRecognizer(transportTap)
        
        let artTap = UITapGestureRecognizer(target: self, action: #selector(artViewTapped(sender: )))
        artStackView.addGestureRecognizer(artTap)
        
        let shoppingTap = UITapGestureRecognizer(target: self, action: #selector(shoppingViewTapped(sender: )))
        shoppingStackView.addGestureRecognizer(shoppingTap)
        
        let tourisamTap = UITapGestureRecognizer(target: self, action: #selector(tourisamViewTapped(sender: )))
        tourisamStackView.addGestureRecognizer(tourisamTap)
        
        let sportTap = UITapGestureRecognizer(target: self, action: #selector(sportViewTapped(sender: )))
        sportStackView.addGestureRecognizer(sportTap)
        
        let generalTap = UITapGestureRecognizer(target: self, action: #selector(generalViewTapped(sender: )))
        generalStackView.addGestureRecognizer(generalTap)
        
        let allTap = UITapGestureRecognizer(target: self, action: #selector(allViewTapped(sender: )))
        allStackView.addGestureRecognizer(allTap)
        
        label1a.text = ""
        label2a.text = ""
        label3a.text = ""
        label4a.text = ""
        label5a.text = ""
        label6a.text = ""
        label7a.text = ""
        label8a.text = ""
        label9a.text = ""
        label10a.text = ""
        label11a.text = ""
        label12a.text = ""
        label13a.text = ""
    }
    
    @objc func containerViewTapped(sender: UITapGestureRecognizer? = nil) {
        self.containerView.endEditing(true)
    }
    
    @objc func aerialViewTapped(sender: UITapGestureRecognizer? = nil) {
        aerialTapped = !aerialTapped
        
        if aerialTapped {
            self.label1a.text = "\u{2713}"
            self.selectedCategories.append(aerialCat)
        } else {
            self.label1a.text = ""
            self.selectedCategories.removeAll(where: { $0 == aerialCat })
        }
    }
    
    @objc func servicesViewTapped(sender: UITapGestureRecognizer? = nil) {
        servicesTapped = !servicesTapped
        
        if servicesTapped {
            self.label2a.text = "\u{2713}"
            self.selectedCategories.append(servicesCat)
        } else {
            self.label2a.text = ""
            self.selectedCategories.removeAll(where: { $0 == servicesCat })
        }
    }
    
    @objc func foodAndDrinkViewTapped(sender: UITapGestureRecognizer? = nil) {
        foodAndDrinkTapped = !foodAndDrinkTapped
        
        if foodAndDrinkTapped {
            self.label3a.text = "\u{2713}"
            self.selectedCategories.append(foodAndDrinkCat)
        } else {
            self.label3a.text = ""
            self.selectedCategories.removeAll(where: { $0 == foodAndDrinkCat })
        }
    }
    
    @objc func hotelViewTapped(sender: UITapGestureRecognizer? = nil) {
        hotelTapped = !hotelTapped
        
        if hotelTapped {
            self.label4a.text = "\u{2713}"
            self.selectedCategories.append(hotelCat)
        } else {
            self.label4a.text = ""
            self.selectedCategories.removeAll(where: { $0 == hotelCat })
        }
    }
    
    @objc func realEstatesViewTapped(sender: UITapGestureRecognizer? = nil) {
        realEstatesTapped = !realEstatesTapped
        
        if realEstatesTapped {
            self.label5a.text = "\u{2713}"
            self.selectedCategories.append(realEstatesCat)
        } else {
            self.label5a.text = ""
            self.selectedCategories.removeAll(where: { $0 == realEstatesCat })
        }
    }
    
    @objc func nightLifeViewTapped(sender: UITapGestureRecognizer? = nil) {
        nightLifeTapped = !nightLifeTapped
        
        if nightLifeTapped {
            self.label6a.text = "\u{2713}"
            self.selectedCategories.append(nightLifeCat)
        } else {
            self.label6a.text = ""
            self.selectedCategories.removeAll(where: { $0 == nightLifeCat })
        }
    }
    
    @objc func transportViewTapped(sender: UITapGestureRecognizer? = nil) {
        transportTapped = !transportTapped
        
        if transportTapped {
            self.label7a.text = "\u{2713}"
            self.selectedCategories.append(transportCat)
        } else {
            self.label7a.text = ""
            self.selectedCategories.removeAll(where: { $0 == transportCat })
        }
    }
    
    @objc func artViewTapped(sender: UITapGestureRecognizer? = nil) {
        artTapped = !artTapped
        
        if artTapped {
            self.label8a.text = "\u{2713}"
            self.selectedCategories.append(artAndCultureCat)
        } else {
            self.label8a.text = ""
            self.selectedCategories.removeAll(where: { $0 == artAndCultureCat })
        }
    }
    
    @objc func shoppingViewTapped(sender: UITapGestureRecognizer? = nil) {
        shoppingTapped = !shoppingTapped
        
        if shoppingTapped {
            self.label9a.text = "\u{2713}"
            self.selectedCategories.append(shopingCat)
        } else {
            self.label9a.text = ""
            self.selectedCategories.removeAll(where: { $0 == shopingCat })
        }
    }
    
    @objc func tourisamViewTapped(sender: UITapGestureRecognizer? = nil) {
        tourisamTapped = !tourisamTapped
        
        if tourisamTapped {
            self.label10a.text = "\u{2713}"
            self.selectedCategories.append(tourismAndAtractionCat)
        } else {
            self.label10a.text = ""
            self.selectedCategories.removeAll(where: { $0 == tourismAndAtractionCat })
        }
    }
    
    @objc func sportViewTapped(sender: UITapGestureRecognizer? = nil) {
        sportTapped = !sportTapped
        
        if sportTapped {
            self.label11a.text = "\u{2713}"
            self.selectedCategories.append(sportAndRecreationCat)
        } else {
            self.label11a.text = ""
            self.selectedCategories.removeAll(where: { $0 == sportAndRecreationCat })
        }
    }
    
    @objc func generalViewTapped(sender: UITapGestureRecognizer? = nil) {
        generalTapped = !generalTapped
        
        if generalTapped {
            self.label12a.text = "\u{2713}"
            self.selectedCategories.append(generalCat)
        } else {
            self.label12a.text = ""
            self.selectedCategories.removeAll(where: { $0 == generalCat })
        }
    }
    
    @objc func allViewTapped(sender: UITapGestureRecognizer? = nil) {
        allTapped = !allTapped
        
        if allTapped {
            self.label1a.text = "\u{2713}"
            self.label2a.text = "\u{2713}"
            self.label3a.text = "\u{2713}"
            self.label4a.text = "\u{2713}"
            self.label5a.text = "\u{2713}"
            self.label6a.text = "\u{2713}"
            self.label7a.text = "\u{2713}"
            self.label8a.text = "\u{2713}"
            self.label9a.text = "\u{2713}"
            self.label10a.text = "\u{2713}"
            self.label11a.text = "\u{2713}"
            self.label12a.text = "\u{2713}"
            self.label13a.text = "\u{2713}"
            self.selectedCategories.removeAll()
            self.selectedCategories.append(aerialCat)
            self.selectedCategories.append(foodAndDrinkCat)
            self.selectedCategories.append(realEstatesCat)
            self.selectedCategories.append(transportCat)
            self.selectedCategories.append(shopingCat)
            self.selectedCategories.append(sportAndRecreationCat)
            self.selectedCategories.append(servicesCat)
            self.selectedCategories.append(hotelCat)
            self.selectedCategories.append(nightLifeCat)
            self.selectedCategories.append(artAndCultureCat)
            self.selectedCategories.append(tourismAndAtractionCat)
            self.selectedCategories.append(generalCat)
            self.settedAllCat = true
            
        } else {
            self.label1a.text = ""
            self.label2a.text = ""
            self.label3a.text = ""
            self.label4a.text = ""
            self.label5a.text = ""
            self.label6a.text = ""
            self.label7a.text = ""
            self.label8a.text = ""
            self.label9a.text = ""
            self.label10a.text = ""
            self.label11a.text = ""
            self.label12a.text = ""
            self.label13a.text = ""
            self.selectedCategories.removeAll(where: { $0 == aerialCat })
            self.selectedCategories.removeAll(where: { $0 == foodAndDrinkCat })
            self.selectedCategories.removeAll(where: { $0 == realEstatesCat })
            self.selectedCategories.removeAll(where: { $0 == transportCat })
            self.selectedCategories.removeAll(where: { $0 == shopingCat })
            self.selectedCategories.removeAll(where: { $0 == sportAndRecreationCat })
            self.selectedCategories.removeAll(where: { $0 == servicesCat })
            self.selectedCategories.removeAll(where: { $0 == hotelCat })
            self.selectedCategories.removeAll(where: { $0 == nightLifeCat })
            self.selectedCategories.removeAll(where: { $0 == artAndCultureCat })
            self.selectedCategories.removeAll(where: { $0 == tourismAndAtractionCat })
            self.selectedCategories.removeAll(where: { $0 == generalCat })
            self.settedAllCat = false
        }
    }
    
    fileprivate func allSelected() {
        allTapped = !allTapped
        
        if allTapped {
            self.label1a.text = "\u{2713}"
            self.label2a.text = "\u{2713}"
            self.label3a.text = "\u{2713}"
            self.label4a.text = "\u{2713}"
            self.label5a.text = "\u{2713}"
            self.label6a.text = "\u{2713}"
            self.label7a.text = "\u{2713}"
            self.label8a.text = "\u{2713}"
            self.label9a.text = "\u{2713}"
            self.label10a.text = "\u{2713}"
            self.label11a.text = "\u{2713}"
            self.label12a.text = "\u{2713}"
            self.label13a.text = "\u{2713}"
            self.selectedCategories.removeAll()
            self.selectedCategories.append(aerialCat)
            self.selectedCategories.append(foodAndDrinkCat)
            self.selectedCategories.append(realEstatesCat)
            self.selectedCategories.append(transportCat)
            self.selectedCategories.append(shopingCat)
            self.selectedCategories.append(sportAndRecreationCat)
            self.selectedCategories.append(servicesCat)
            self.selectedCategories.append(hotelCat)
            self.selectedCategories.append(nightLifeCat)
            self.selectedCategories.append(artAndCultureCat)
            self.selectedCategories.append(tourismAndAtractionCat)
            self.selectedCategories.append(generalCat)
            self.settedAllCat = true
            
        } else {
            self.label1a.text = ""
            self.label2a.text = ""
            self.label3a.text = ""
            self.label4a.text = ""
            self.label5a.text = ""
            self.label6a.text = ""
            self.label7a.text = ""
            self.label8a.text = ""
            self.label9a.text = ""
            self.label10a.text = ""
            self.label11a.text = ""
            self.label12a.text = ""
            self.label13a.text = ""
            self.selectedCategories.removeAll(where: { $0 == aerialCat })
            self.selectedCategories.removeAll(where: { $0 == foodAndDrinkCat })
            self.selectedCategories.removeAll(where: { $0 == realEstatesCat })
            self.selectedCategories.removeAll(where: { $0 == transportCat })
            self.selectedCategories.removeAll(where: { $0 == shopingCat })
            self.selectedCategories.removeAll(where: { $0 == sportAndRecreationCat })
            self.selectedCategories.removeAll(where: { $0 == servicesCat })
            self.selectedCategories.removeAll(where: { $0 == hotelCat })
            self.selectedCategories.removeAll(where: { $0 == nightLifeCat })
            self.selectedCategories.removeAll(where: { $0 == artAndCultureCat })
            self.selectedCategories.removeAll(where: { $0 == tourismAndAtractionCat })
            self.selectedCategories.removeAll(where: { $0 == generalCat })
            self.settedAllCat = false
        }
    }
    
    // MARK: - API
    
    func loadSearchData(completion: @escaping (Int) -> Void) {
        var url = ""
        if let searchCriteria = searchTextField.text {
            if settedAllCat {
                url = "https://api.cityguide360.ba:1337/panorama/search/\(searchCriteria)/all"
            } else {
                let filter = selectedCategories.joined(separator: ",")
                url = "https://api.cityguide360.ba:1337/panorama/search/\(searchCriteria)/\(filter)"
            }
            
            print("URL:", url)
            Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
                if response.result.isSuccess {
                    print("Success! Got the data")
                    
                    let searchJSON: JSON = JSON(response.result.value!)
                    var i = 0
                    for _ in searchJSON {
                        let name = searchJSON[i]["name"].stringValue
                        let thumbnail = searchJSON[i]["thumbnail"].stringValue
                        let thumbnailReady = "http://imagery.cityguide360.ba/\(thumbnail)"
                        let panoramaId = searchJSON[i]["id"].stringValue
                        let categoryName = searchJSON[i]["category"]["id"].stringValue
                        let description = searchJSON[i]["description"].stringValue
                        let descriptionEn = searchJSON[i]["descriptionEn"].stringValue
                        let descriptionDe = searchJSON[i]["descriptionDe"].stringValue
                        let sound = searchJSON[i]["sound"].stringValue
                        
                        let sm = Search(categoryName: categoryName, name: name, thumbnail: thumbnailReady, id: panoramaId, sound: sound, description: description, descriptionEn: descriptionEn, descriptionDe: descriptionDe)
                        self.searchArray.append(sm)
                        
                        i += 1
                    }
                    completion(i)
                    self.indicatorView.stopAnimating()
                } else {
                    print(response.result.error as Any)
                }
            }
            
        }
        
    }
    
    func loadSearchDataHome(completion: @escaping (Int) -> Void) {
            var url = ""
            url = "https://api.cityguide360.ba:1337/panorama/map"
    
            print("URL:", url)
            Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
                if response.result.isSuccess {
                    print("Success! Got the data")
                    
                    let searchJSON: JSON = JSON(response.result.value!)
                    var i = 0
                    for _ in searchJSON {
                        let name = searchJSON[i]["name"].stringValue
                        let thumbnail = searchJSON[i]["thumbnail"].stringValue
                        let thumbnailReady = "http://imagery.cityguide360.ba/\(thumbnail)"
                        let panoramaId = searchJSON[i]["id"].stringValue
                        let categoryName = searchJSON[i]["category"].stringValue
                        let description = searchJSON[i]["description"].stringValue
                        let descriptionEn = searchJSON[i]["descriptionEn"].stringValue
                        let descriptionDe = searchJSON[i]["descriptionDe"].stringValue
                        let sound = searchJSON[i]["sound"].stringValue
                        
                        let sm = Search(categoryName: categoryName, name: name, thumbnail: thumbnailReady, id: panoramaId, sound: sound, description: description, descriptionEn: descriptionEn, descriptionDe: descriptionDe)
                        self.searchArray.append(sm)
                        
                        i += 1
                    }
                    completion(i)
                    self.indicatorView.stopAnimating()
                } else {
                    print(response.result.error as Any)
                }
            }
            
        }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailFromSearch" {
            let destinationVc = segue.destination as! DetailWebViewController
            destinationVc.panoramaId = pomPanoramaId
        }
        
        
    }
 

}
// MARK: - TableView Delegate & Datasource

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchTableViewCell
        
        cell.searchModel = searchArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        pomPanoramaId = searchArray[indexPath.row].id
        performSegue(withIdentifier: "toDetailFromSearch", sender: self)
        tableView.deselectRow(at: indexPath, animated: false)

    }
}

// MARK: - TextField Delegate

extension SearchViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var result = false
        
        if textField.text == "" {
            let Alert = UIAlertController(title: localizer[Messages.warning], message: localizer[Messages.searchCriteria], preferredStyle: .alert)
            
            let DismisButton = UIAlertAction(title: localizer[Messages.dismis], style: .cancel, handler: {
                (alert:UIAlertAction!) -> Void in
            })
            
            Alert.addAction(DismisButton)
            
            self.present(Alert, animated: true, completion: nil)
            result = false
        } else {
            
            if selectedCategories.count > 0 {
                self.indicatorView.startAnimating()
                self.searchArray.removeAll()
                loadSearchData { (count) in
                    print("Count:", count as Any)
                    if count == 0 {
                        self.tableView.setEmptyMessage(self.localizer[Messages.noResults])
                        self.indicatorView.stopAnimating()
                    }
                    
                    self.containerView.gestureRecognizers?.removeAll()
                    self.tableView.reloadData()
                }
            } else {
               
                self.alert(message: localizer[Messages.mustChooseCategory])
            }
            
            textField.resignFirstResponder()
            result = true
        }
        
        return result
    }

    
}


