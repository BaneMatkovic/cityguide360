//
//  Video360ViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 03/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
import WebKit

class Video360ViewController: UIViewController {

    @IBOutlet var playerView: WKYTPlayerView!
    var link = ""
    
    var youtube = "TjNQ_7sCPV4"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.playerView.load(withVideoId: link)
    }
    
    
}
