//
//  DetailWebViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 15/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import WebKit

class DetailWebViewController: UIViewController, WKScriptMessageHandler {

    // MARK: - Properties
    
    var webView: WKWebView!
    
    var panoramaId: String = ""
    var pomPanoramaId = ""
    var photo360flag = 0
    var nearByFlag = 0
    var nearByListFlag = 0
    
    fileprivate let actionButton = JJFloatingActionButton()
    fileprivate let actionButtonInfo = JJFloatingActionButton()
    
    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupWebView()
        self.view.addSubview(self.webView)
        
        if photo360flag == 1 || nearByFlag == 1 || nearByListFlag == 1 {
            addInfoButton()
        } else {
        addActionButton()
        addInfoButton()
        }
        
        if let url = URL(string: "https://ios.cityguide360.ba/panorama/\(panoramaId)/search/ba/") {
            
            let request = URLRequest(url: url)
            self.webView.load(request)
        }
        
        
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func setupWebView() {
        
        let contentController = WKUserContentController()
        let userScript = WKUserScript(
            source: "mobileHeader()",
            injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.add(self, name: "loginAction")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        
    }
    
    // MARK: - Actions
    
    fileprivate func addActionButton() {
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonColor = UIColor(hexString: "#121925")
        actionButton.buttonImageColor = .white
        actionButton.addItem(title: "", image: UIImage(named: "back2")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("back button clicked")
            self.dismiss(animated: true, completion: nil)
            //self.performSegue(withIdentifier: "showReveal0", sender: self)
            
        }
        actionButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 50).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -52).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    fileprivate func addInfoButton() {
        view.addSubview(actionButtonInfo)
        actionButtonInfo.translatesAutoresizingMaskIntoConstraints = false
        actionButtonInfo.overlayView.backgroundColor = .clear
        actionButtonInfo.buttonColor = UIColor(hexString: "#121925")
        actionButtonInfo.buttonImageColor = .white
        actionButtonInfo.addItem(title: "", image: UIImage(named: "info")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("info button clicked")
            self.performSegue(withIdentifier: "showInfo2", sender: self)
            
        }
        actionButtonInfo.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50).isActive = true
        actionButtonInfo.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -52).isActive = true
        actionButtonInfo.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInfo2" {
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
            let navController = UINavigationController(rootViewController: vc)
            
            vc.panoramaId = panoramaId //pomPanoramaId
            self.present(navController, animated:true, completion: nil)
        }
    }
    
    // MARK: - WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "loginAction" {
            panoramaId = message.body as! String //pomPanoramaId
            print("JavaScript is sending a message from detaul: \(message.body)")
        }
    }
}
