//
//  HomeViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import iOSDropDown

class HomeViewController: UIViewController {

     // MARK: - Properties
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var palleteView: UIView!
    @IBOutlet weak var manuImageView: UIImageView!
    @IBOutlet weak var upOneView: UIView!
    @IBOutlet weak var upTwoView: UIView!
    @IBOutlet weak var upThreeView: UIView!
    @IBOutlet weak var upOneCatView: UIView!
    @IBOutlet weak var upTwoCatView: UIView!
    @IBOutlet weak var upThreeCatView: UIView!
    @IBOutlet weak var upFourCatView: UIView!
    @IBOutlet weak var upFiveCatView: UIView!
    @IBOutlet weak var upSixCatView: UIView!
    @IBOutlet weak var upSevenCatView: UIView!
    @IBOutlet weak var upEightCatView: UIView!
    @IBOutlet weak var upNineCatView: UIView!
    @IBOutlet weak var upTenCatView: UIView!
    @IBOutlet weak var upElevenCatView: UIView!
    @IBOutlet weak var upTwelveCatView: UIView!
    @IBOutlet weak var eventsView: UIView!
    @IBOutlet weak var allCategoriesView: AllCategoriesView!
    
    
    var localizer = LocalizationManager.shared
    
    let searchTextField = SearchTextField()
    let reachability = Reachability()!
    
    let dropDown = DropDown()
    
    fileprivate func addPalleteView() {
        let menuImage = UIImage(named: "icon_menu")?.withRenderingMode(.alwaysTemplate)
        let searchImage = UIImage(named: "searchFancy")?.withRenderingMode(.alwaysTemplate)
        menuButton.setImage(menuImage, for: .normal)
        searchButton.setImage(searchImage, for: .normal)
        menuButton.tintColor = Theme.Colors.secondaryTextColor
        searchButton.tintColor = Theme.Colors.secondaryTextColor
        manuImageView.image = UIImage(named: "logo")
        manuImageView.contentMode = .scaleAspectFit
        manuImageView.isHidden = true
    
        palleteView.layer.cornerRadius = 2
        palleteView.layer.masksToBounds = true
        
        /*
        palleteView.addSubview(searchTextField)
        searchTextField.delegate = self
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.leftAnchor.constraint(equalTo: palleteView.leftAnchor, constant: Theme.Dimensions.padding).isActive = true
        searchTextField.rightAnchor.constraint(equalTo: searchButton.leftAnchor, constant: -20).isActive = true
        searchTextField.centerYAnchor.constraint(equalTo: palleteView.centerYAnchor).isActive = true
        searchTextField.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        */
        
        view.addSubview(dropDown)
        dropDown.translatesAutoresizingMaskIntoConstraints = false
        dropDown.leftAnchor.constraint(equalTo: palleteView.leftAnchor, constant: Theme.Dimensions.padding + 30).isActive = true
        dropDown.rightAnchor.constraint(equalTo: searchButton.rightAnchor, constant: -40).isActive = true
        dropDown.centerYAnchor.constraint(equalTo: palleteView.centerYAnchor).isActive = true
        //dropDown.centerXAnchor.constraint(equalTo: palleteView.centerXAnchor).isActive = true
        dropDown.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        dropDown.textAlignment = .center
        dropDown.placeholder = localizer[Placeholders.clickToChooseCity]
        
    }

    fileprivate func showSideMenu() {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        //self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }
    
    // MARK: Tap Gestures
    
    fileprivate func upOneViewTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upOneViewTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upOneView.addGestureRecognizer(tap)
    }
    
    fileprivate func upTwoViewTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upTwoViewTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upTwoView.addGestureRecognizer(tap)
    }
    
    fileprivate func upThreeViewTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upThreeViewTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upThreeView.addGestureRecognizer(tap)
    }
    
    fileprivate func upOneCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upOneCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upTwoCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upTwoCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upTwoCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upThreeCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upThreeCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upThreeCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upFourCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upFourCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upFourCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upFiveCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upFiveCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upFiveCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upSixCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upSixCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upSixCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upSevenCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upSevenCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upSevenCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upEightCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upEightCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upEightCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upNineCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upNineCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upNineCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upTenCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upTenCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upTenCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upElevenCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upElevenCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upElevenCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upTwelveCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upTwelveCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        upTwelveCatView.addGestureRecognizer(tap)
    }
    
    fileprivate func upAllCatTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(upAllCatTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        allCategoriesView.addGestureRecognizer(tap)
    }
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        didBecomeActiveCheck()
        
        addPalleteView()
        
        showSideMenu()
        
        upOneViewTapGesture()
        upTwoViewTapGesture()
        upThreeViewTapGesture()
        
        upOneCatTapGesture()
        upTwoCatTapGesture()
        upThreeCatTapGesture()
        upFourCatTapGesture()
        upFiveCatTapGesture()
        upSixCatTapGesture()
        upSevenCatTapGesture()
        upEightCatTapGesture()
        upNineCatTapGesture()
        upTenCatTapGesture()
        upElevenCatTapGesture()
        upTwelveCatTapGesture()
        upAllCatTapGesture()
        
        dropDown.arrowSize = 10
        dropDown.optionArray = ["TUZLA", "BIJELJINA", "BRCKO"]
        dropDown.optionIds = [0, 1, 2]
        dropDown.arrowSize = 0
        dropDown.didSelect { (selectedText, index, id) in
            let defaults = UserDefaults.standard
            defaults.set(selectedText, forKey: "city")
            print("selected text:", selectedText)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setReachabilityNotifier()
        AppUtility.lockOrientation(.portrait)
        localize()
        
        let defaults = UserDefaults.standard
        let city = defaults.string(forKey: "city")
        
        if city != "" {
            print(city as Any)
            dropDown.text = city?.uppercased()
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.all)
    }
    
    //MARK: - Actions
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    */
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {

        print("search button tapeped")
        
    }
    
    @objc func upOneViewTapped(sender: UITapGestureRecognizer? = nil){
        present(UpOneViewController(), animated: true, completion: nil)
    }
    
    @objc func upTwoViewTapped(sender: UITapGestureRecognizer? = nil){
        present(UpOneViewController(), animated: true, completion: nil)
    }
    
    @objc func upThreeViewTapped(sender: UITapGestureRecognizer? = nil){
        present(UpOneViewController(), animated: true, completion: nil)
    }
    
    @objc func upCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upOneCat", sender: self)
    }
    
    @objc func upTwoCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upTwoCat", sender: self)
    }
    
    @objc func upThreeCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upThreeCat", sender: self)
    }
    
    @objc func upFourCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upFourCat", sender: self)
    }
    
    @objc func upFiveCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upFiveCat", sender: self)
    }
    
    @objc func upSixCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upSixCat", sender: self)
    }
    
    @objc func upSevenCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upSevenCat", sender: self)
    }
    
    @objc func upEightCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upEightCat", sender: self)
    }
    
    @objc func upNineCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upNineCat", sender: self)
    }
    
    @objc func upTenCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upTenCat", sender: self)
    }
    
    @objc func upElevenCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upElevenCat", sender: self)
    }
    
    @objc func upTwelveCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upTwelveCat", sender: self)
    }
    
    @objc func upAllCatTapped(sender: UITapGestureRecognizer? = nil){
        performSegue(withIdentifier: "upAllCat", sender: self)
    }

    // MARK: Localization
    fileprivate func localize() {
        let defaults = UserDefaults.standard
        let locale = defaults.string(forKey: "locale")
        if let language = Language(rawValue: locale ?? "English") {
            localizer.changeLanguage(language)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "upOneCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = aerialCat //"aerial"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upTwoCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = foodAndDrinkCat //"foodAndDrink"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upThreeCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = realEstatesCat //"realEstates"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upFourCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = transportCat //"transport"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upFiveCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = shopingCat //"shoping"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upSixCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = sportAndRecreationCat//"Sport&Recreation"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upSevenCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = servicesCat //"Services"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upEightCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = hotelCat //"Hotel"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upNineCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = nightLifeCat //"NightLife"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upTenCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = artAndCultureCat //"ArtAndCulture"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upElevenCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = tourismAndAtractionCat //"TurismAndAtractions"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upTwelveCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = generalCat //"General"
            self.present(controller, animated: true, completion: nil)
        }
        
        if segue.identifier == "upAllCat" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "reveal2")
            category = "sve" //"General"
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    
    
}

extension HomeViewController: UITextFieldDelegate {
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let text = textField.text {
            print(text)
            
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchTextField.text = ""
        return true
    }
    
    fileprivate func searcTextFieldBottomBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: self.searchTextField.frame.size.height - width, width: self.searchTextField.frame.size.width, height: self.searchTextField.frame.size.height)
        border.borderWidth = width
        self.searchTextField.layer.addSublayer(border)
        self.searchTextField.layer.masksToBounds = true
        self.searchTextField.placeholder = localizer[Placeholders.search]
    }
    

    
}

extension HomeViewController: InternetObserver {
    // MARK: - Internet Observer
    func setReachabilityNotifier() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("could not start notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        switch reachability.connection {
        case .wifi:
            print("via Wifi")
        case .cellular:
            print("via cellua")
        case .none:
            print("Network not reachable")
            self.alert(message: "Check Internet connection!", title: "Warning")
        }
    }
    
    func didBecomeActiveCheck() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged(note:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }
    
}
