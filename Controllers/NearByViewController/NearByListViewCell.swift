//
//  NearByListViewCell.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 20/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import SDWebImage
import CoreData

class NearByListViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var panoramaId = ""
    var name = ""
    var imageUrl = ""
    
    let actionButtonInfo = JJFloatingActionButton()
    
    var favoritesList: [Favorites] = []
    
    var listModel: List! {
        didSet {
            nameLabel.text = listModel.name
            
            let pictureUrl = URL(string: listModel.thumbnail)
            let placeholder = UIImage(named: "Slika 1")
            cellImageView.sd_setImage(with: pictureUrl, placeholderImage: placeholder)
            
            panoramaId = listModel.panoramaId
            name = listModel.name
            imageUrl = listModel.thumbnail
        }
    }
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addInfoButton()
        self.actionButtonInfo.buttonImageColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    fileprivate func addInfoButton() {
        addSubview(actionButtonInfo)
        actionButtonInfo.translatesAutoresizingMaskIntoConstraints = false
        actionButtonInfo.overlayView.backgroundColor = .clear
        actionButtonInfo.buttonColor = UIColor(hexString: "#121925")
        actionButtonInfo.buttonImageColor = .white
        actionButtonInfo.addItem(title: "", image: UIImage(named: "heart3")?.withRenderingMode(.alwaysTemplate)) { item in
            self.actionButtonInfo.tintColor = .white
            print("info button clicked")
            let newItem = Favorites(context: self.context)
            newItem.name = self.name
            newItem.panoramaId = self.panoramaId
            newItem.imageUrl = self.imageUrl
            //print("Postoji:",self.someEntityExists(id: self.panoramaId))
            /*
             if let img = UIImage(named: "Slika 1") {
             let data = img.pngData() as Data?
             newItem.image = data
             }
             */
            self.favoritesList.append(newItem)
            self.saveItems()
            
            //self.delegate?.dataBackToVC(text: "Reddd")
            self.actionButtonInfo.buttonImageColor = .red
            self.layoutIfNeeded()
            self.reloadInputViews()
            
        }
        actionButtonInfo.rightAnchor.constraint(equalTo: rightAnchor, constant: -30).isActive = true
        actionButtonInfo.topAnchor.constraint(equalTo: topAnchor, constant: 32).isActive = true
        actionButtonInfo.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    // MARK: - CoreData
    
    func saveItems() {
        
        do {
            try context.save()
        } catch {
            print(error)
        }
    }
    
    func someEntityExists(id: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Favorites")
        fetchRequest.predicate = NSPredicate(format: "panoramaId = %@", id)
        
        var results: [NSManagedObject] = []
        
        do {
            results = try context.fetch(fetchRequest)
            print(results)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return results.count > 0
    }

}
