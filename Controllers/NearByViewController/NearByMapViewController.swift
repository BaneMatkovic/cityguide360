//
//  NearByMapViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

class NearByMapViewController: UIViewController, GMSMapViewDelegate{

    // MARK: Properties
    @IBOutlet weak var spinnerView: NVActivityIndicatorView!
    @IBOutlet weak var containerView: GMSMapView!
    
    var mapView = GMSMapView()
    let infoMarker = GMSMarker()
    var hotSpot: [HotSpot] = []
    var latitude: Double = 0.0
    var pomPanoramaId = ""
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinnerView.startAnimating()
        //displayMap(lat: nearByLatitude, long: nearByLongitude)
        let defaults = UserDefaults.standard
        let city = defaults.string(forKey: "city") ?? ""
        let contain = cities.contains(city)
        if contain {
            switch city {
            case "TUZLA":
                displayMap(lat: tuzlaCity.lat, long: tuzlaCity.long)
            default:
                displayMap(lat: tuzlaCity.lat, long: tuzlaCity.long)
            }
        } else {
            displayMap(lat: tuzlaCity.lat, long: tuzlaCity.long)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        containerView.selectedMarker = nil
    }
    
    fileprivate func displayMap(lat: Double, long: Double) {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12.0) ///14.0
        containerView.animate(to: camera)
        containerView.delegate = self
        containerView.mapType = .normal
        //mapView.mapType = .satellite
        
        getHotSpots(category: "") { (result) in
            print(result)
        }
        spinnerView.stopAnimating()
    }
    
    func getHotSpots(category: String, completion: @escaping (Int) -> Void) {
        print("nearByPanoramaId:", nearByPanoramaId)
        let url = "https://api.cityguide360.ba:1337/panorama/distance/\(nearByPanoramaId)"
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                let hotSpotJSON: JSON = JSON(response.result.value!)
                var j = 0
                var i = 0
                for _ in hotSpotJSON {
                    let lat = hotSpotJSON[i]["lat"].doubleValue
                    let lon = hotSpotJSON[i]["lon"].doubleValue
                    let id = hotSpotJSON[i]["id"].stringValue
                    let myCategory = hotSpotJSON[i]["category"].stringValue
                    let hs = HotSpot(lat: lat, long: lon, id: id, category: myCategory)
                    if lat != 0 {
                        self.hotSpot.append(hs)
                        let mark = GMSMarker()
                        mark.position = CLLocationCoordinate2D(latitude: hs.lat, longitude: hs.long)
                        mark.title = hs.id
                        mark.icon = UIImage(named: hs.category)
                        mark.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
                        mark.map = self.containerView
                        j += 1
                    }
                    i += 1
                }
                completion(j)
                let mark = GMSMarker()
                mark.position = CLLocationCoordinate2D(latitude: nearByLatitude, longitude: nearByLongitude)
                mark.icon = UIImage(named: "poi1")
                mark.map = self.containerView
            } else {
                print(response.result.error as Any)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        let view = UIView()
        
        pomPanoramaId = marker.title ?? ""
        self.performSegue(withIdentifier: "fromNearBy", sender: self)
        
        return view
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromNearBy" {
            let destinationVc = segue.destination as! DetailWebViewController
            destinationVc.panoramaId = pomPanoramaId
            destinationVc.nearByFlag = 1
        }
    }

}
