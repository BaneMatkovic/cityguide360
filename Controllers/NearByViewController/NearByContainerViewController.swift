//
//  NearByContainerViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

var nearByLatitude = 0.0
var nearByLongitude = 0.0
class NearByContainerViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var segmentedView: UISegmentedControl!
    @IBOutlet weak var containerA: UIView!
    @IBOutlet weak var containerB: UIView!
    @IBOutlet weak var upView: UIView!
    var latitude: Double = 0
    var longitude: Double = 0
    
    var localizer = LocalizationManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedView.setTitle(localizer[Segment.map], forSegmentAt: 0)
        segmentedView.setTitle(localizer[Segment.listView], forSegmentAt: 1)
        
        nearByLatitude = latitude
        nearByLongitude = longitude
        
        let backImage = UIImage(named: "back2")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.backItem?.title = ""
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        
    }
    

    //MARK: - Appearance
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: Actions
    
    @IBAction func segmentedAction(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.containerA.alpha = 1
                self.containerB.alpha = 0
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.containerA.alpha = 0
                self.containerB.alpha = 1
            })
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
