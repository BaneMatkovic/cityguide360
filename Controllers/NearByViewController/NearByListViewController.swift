//
//  NearByListViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NearByListViewController: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    var listArray: [List] = []
    var pomPanoramaId: String = ""
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = .white
        
        populateTable { (count) in
            print("count:", count)
            self.tableView.reloadData()
        }

    }
    
    // MARK: API
    
    func populateTable(completion: @escaping (Int) -> Void) {
        let url = "https://api.cityguide360.ba:1337/panorama/distance/\(nearByPanoramaId)"
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                
                let hotSpotJSON: JSON = JSON(response.result.value!)
                var i = 0
                for _ in hotSpotJSON {
                    let name = hotSpotJSON[i]["name"].stringValue
                    let thumbnail = hotSpotJSON[i]["thumbnail"].stringValue
                    let thumbnailReady = "https://imagery.cityguide360.ba/\(thumbnail)"
                    let panoramaId = hotSpotJSON[i]["id"].stringValue
                    let lm = List(name: name, thumbnail: thumbnailReady, panoramaId: panoramaId)
                    
                    self.listArray.append(lm)
                    i += 1
                }
                completion(i)
            } else {
                print(response.result.error as Any)
            }
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromNearByList" {
            let destinationVc = segue.destination as! DetailWebViewController
            destinationVc.panoramaId = pomPanoramaId
            destinationVc.nearByListFlag = 1
        }
    }

}

extension NearByListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NearByListViewCell
        
        DispatchQueue.main.async {
            cell.listModel = self.listArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pomPanoramaId = listArray[indexPath.row].panoramaId
        //heartArray[indexPath.row] = "Red"
        performSegue(withIdentifier: "fromNearByList", sender: self)
    }
}
