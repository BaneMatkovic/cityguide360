//
//  Photo360CollectionViewCell.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 17/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit

class Photo360CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    
}
