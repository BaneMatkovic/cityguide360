//
//  Photo360ViewController.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 17/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces

class Photo360ViewController: UIViewController, GMSMapViewDelegate {

    // MARK: Properties
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var globeButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var myView: GMSMapView!
    
    var group = ""
    var images360: [String] = []
    var panoramas: [String] = []
    var name = ""
    var latitude: Double = 0
    var longitude: Double = 0
    
    let infoMarker = GMSMarker()
    var hotSpot: [HotSpot] = []
    var panoramaSelected = ""
    
    var favoritesList: [Favorites] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        myView.isHidden = true
        configure()
        //displayMap(lat: latitude, long: longitude)
        load360Photos(groupId: group) { (data) in
            print(data)
        }
    }
    
    // MARK: Apperance
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    fileprivate func configure() {
        let backImage = UIImage(named: "back2")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.backItem?.title = ""
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        
        likeButton.setImage(UIImage(named: "heart3")?.withRenderingMode(.alwaysTemplate), for: .normal)
        likeButton.tintColor = .white
    
        globeButton.setImage(UIImage(named: "globe2")?.withRenderingMode(.alwaysTemplate), for: .normal)
        globeButton.tintColor = .white
        
        
    }
    
    // MARK: API
    
    func load360Photos(groupId: String,completion: @escaping (String) -> Void) {
        let url = "https://api.cityguide360.ba:1337/panorama/group/\(groupId)"
        print("group id:", groupId)
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            if response.result.isSuccess {
                print("Success! Got the data")
                let panJSON: JSON = JSON(response.result.value!)
                //print("360Photos:",panJSON)
                
                 var i = 0
                 for _ in panJSON{
                 let images = panJSON[i]["thumbnail"].stringValue
                 let panoramaId = panJSON[i]["id"].stringValue
                    print("panoramaiD:", panoramaId)
                 self.panoramas.append(panoramaId)
                    
                 if images != "" {
                 let readyImage = "https://imagery.cityguide360.ba/\(images)"
                    print("readyImage:", readyImage)
                 self.images360.append(readyImage)
                 }
                    
                 let nam = panJSON[i]["name"].stringValue
                 self.name = nam
                 self.nameLabel.text = nam
                 i += 1
                 }
                completion("test")
                self.collectionView.reloadData()
            } else {
                print(response.result.error as Any)
            }
        }
    }
    
    fileprivate func displayMap(lat: Double, long: Double) {
        print("latitude:", lat)
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 19.0)
        //infoMarker.position.latitude = self.latitude
        //infoMarker.position.longitude = self.longitude
        self.myView.animate(to: camera)
        self.myView.delegate = self
        self.myView.mapType = .satellite
        let mark = GMSMarker()
        mark.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        mark.map = myView
        self.myView = GMSMapView.map(withFrame: .zero, camera: camera)
    }

    // MARK: Actions
    
    @IBAction func globeButtonTapped(_ sender: Any) {
        self.myView.isHidden = false
        displayMap(lat: latitude, long: longitude)
    }
    
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        let newItem = Favorites(context: self.context)
        newItem.name = self.name
        newItem.panoramaId = self.panoramas.first
        newItem.imageUrl = self.images360.first
 
        self.favoritesList.append(newItem)
        self.saveItems()
        
        likeButton.tintColor = .red
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailFrom360Photos" {
            let destinationVC = segue.destination as! DetailWebViewController
            destinationVC.panoramaId = panoramaSelected
            destinationVC.photo360flag = 1
        }
    }
    
    // MARK: - CoreData
    
    func saveItems() {
        
        do {
            try context.save()
        } catch {
            print(error)
        }
    }
    
}

extension Photo360ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images360.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! Photo360CollectionViewCell
        let pictureUrl = URL(string: images360[indexPath.item])
        let placeholder = UIImage(named: "Slika 1")
        cell.cellImageView.sd_setImage(with: pictureUrl, placeholderImage: placeholder)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let panSelected = panoramas[indexPath.item]
        panoramaSelected = panSelected
        print("pan seletced:", panSelected)
        performSegue(withIdentifier: "toDetailFrom360Photos", sender: self)
    }
    
}
