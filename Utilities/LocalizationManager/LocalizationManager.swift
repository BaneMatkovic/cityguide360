//
//  LocalizationManager.swift
//  60seconds
//
//  Created by Djuro Alfirevic on 9/6/18.
//  Copyright © 2018 Djuro Alfirevic. All rights reserved.
//

import Foundation

let LANGUAGE_KEY = "LanguageKey"

enum Language: String {
    case english = "English"
    case bosnian = "Bosnian"
    case deutchland = "Deutchland"
    
    var locale: String {
        switch self {
        case .english:
            return "en"
        case .bosnian:
            return "ba"
        case .deutchland:
            return "de"
        }
    }
    
}

class LocalizationManager {
    
    // MARK: - Properties
    static let shared = LocalizationManager()
    private let defaults = UserDefaults.standard
    private var dictionary = [String: String]()
    
    subscript(key: String) -> String {
        //print("BOOO ", key)
        return localizedString(for: key)
        //return localizedStringFromBundle(for: key)
    }
    
    // MARK: - Initializer
    private init() {}
    
    // MARK: - Public API
    func changeLanguage(_ language: Language) {
        defaults.set(language.rawValue, forKey: LANGUAGE_KEY)
        defaults.synchronize()
        
        if let path = Bundle.main.path(forResource: language.rawValue, ofType: "json") {
            //print("Lanhuage in Localization Manager:",language.rawValue)
            let url = URL(fileURLWithPath: path)
            let data = try? Data(contentsOf: url, options: .mappedIfSafe)
            
            if let data = data {
                let result = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                
                if let result = result as? [String: String] {
                    dictionary = result
                    //print("Lanhuage dictionary:", dictionary)
                }
            }
        }
        
        // Inform all view controllers to update their outlets
        NotificationCenter.default.post(name: NSNotification.Name(Notifications.localizeNotification), object: nil)
    }
    
    func localizedString(for key: String) -> String {
        return dictionary[key] ?? key
    }
    
    func localizedStringFromBundle(for key: String) -> String {
        guard let string = defaults.value(forKey: LANGUAGE_KEY) as? String else { return "" }
        
        if let language = Language(rawValue: string) {
            if let resource = Bundle.main.path(forResource: language.locale, ofType: "lproj") {
                if let path = Bundle(path: resource) {
                    return path.localizedString(forKey: key, value: nil, table: nil)
                }
                
                return ""
            }
        }
        
        return ""
    }
    
    func configure() {
        if defaults.object(forKey: LANGUAGE_KEY) == nil {
            defaults.set(Language.english.rawValue, forKey: LANGUAGE_KEY)
            defaults.synchronize()
            
            changeLanguage(Language.english)
        } else {
            if let string = defaults.value(forKey: LANGUAGE_KEY) as? String {
                if let language = Language(rawValue: string) {
                    changeLanguage(language)
                }
            }
        }
    }
    
    func test() {
        changeLanguage(.english)
        Logger.log(message: localizedStringFromBundle(for: "Password"), type: .debug)
        
        LocalizationManager.shared.changeLanguage(.bosnian)
        Logger.log(message: localizedStringFromBundle(for: "Password"), type: .debug)
    }
    
    // MARK: - Private API
    fileprivate func log() {
        guard let string = defaults.value(forKey: LANGUAGE_KEY) as? String else { return }
        
        if let language = Language(rawValue: string) {
            Logger.log(message: "Language: \(language.rawValue), locale: \(language.locale)", type: .info)
        }
    }
    
    // MARK: My function for localize

    
}
