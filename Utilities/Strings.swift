//
//  Strings.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

struct Messages {
    static let warning = "Warning"
    static let searchCriteria = "Please enter search criteria"
    static let dismis = "Dismiss"
    static let noResults = "No results"
    static let noDetails = "No details for this panorama"
    static let shareApp = "Share 360CityGuide app"
    static let mustChooseCategory = "You have to select a category"
}

struct Placeholders {
    static let search = "Search"
    static let clickToChooseCity = "Click to choose city"
}

struct Labels {
    static let firstView = "First View Text"
    static let foodAndDrink = "Food & Drink"
    static let populare = "Populare"
    static let streets = "Streets"
    static let aerial = "Aerial"
    static let realEstates = "Real Estates"
    static let transport = "Transport"
    static let shopping = "Shopping"
    static let sportAndRecreation = "Sport & Recreation"
    static let services = "Services"
    static let hotel = "Hotel"
    static let nightLife = "Night Life"
    static let artAndCulture = "Art & Culture"
    static let tourismAndAttractions = "Tourism & Attractions"
    static let general = "General"
    static let events = "Events"
    static let allCategories = "All Categories"
}

struct Buttons {
    static let home = "     Home"
    static let contactUs = "    Contact Us"
    static let sendFeedbback = "    Send Feedback"
    static let help = "    Help"
    static let myFavourites = "    My favourites"
    static let photo360 = "360 Photo"
    static let video360 = "360 Video"
    static let photo = "Photo"
    static let navigation = "Navigation"
    static let nearBy = "Near By"
    static let share = "Share"
    
}

struct Notifications {
    static let openSideMenu = "OpenSideMenuNotification"
    static let closeSideMenu = "CloseSideMenuNotification"
    static let openViewController = "OpenViewControllerNotification"
    static let openRootViewController = "OpenRootViewControllerNotification"
    static let localizeNotification = "LocalizeNotification"
    static let completeSocialLoginNotification = "CompleteSocialLoginNotification"
    static let refreshPurchasingSummaryNotification = "RefreshPurchasingSummaryNotification"
    static let presentViewController = "PresentViewControllerNotification"
}

struct Segment {
    static let map = "Map"
    static let listView = "List View"
}

