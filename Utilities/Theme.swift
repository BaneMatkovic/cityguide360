//
//  Theme.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation
import UIKit

struct Theme {
    
    struct Dimensions {
        static let separatorLineHeight: CGFloat = 2.0
        static let borderWidth: CGFloat = 2.0
        static let buttonHeight: CGFloat = 50.0
        static let buttonWidth: CGFloat = 120.0
        static let headerHeight: CGFloat = UIDevice.current.iPhoneX ? 90.0 : 60.0
        static let categoriesHeight: CGFloat = 50.0
        static let sideMenuOffset: CGFloat = 80.0
        static let tableRowHeight: CGFloat = 40.0
        static let padding: CGFloat = 20.0
    }
    
    struct Constants {
        static let overlayAlpha: CGFloat = 0.65
    }
    
    struct Animations {
        static let duration: TimeInterval = 0.5
        static let dampingRatio: CGFloat = 0.3
    }
    
    struct Colors {
        static let primary = UIColor(named: "Primary Color")!
        static let primaryDark = UIColor(named: "Primary Dark Color")!
        static let google = UIColor(named: "Google Color")!
        static let facebook = UIColor(named: "Facebook Color")!
        static let primaryTextColor = UIColor(named: "Primary Text Color")!
        static let secondaryTextColor = UIColor(named: "Secondary Text Color")!
        static let success = UIColor(named: "Success Color")!
        static let error = UIColor(named: "Error Color")!
    }
    
    struct InfoColors {
        static let infoBackground = UIColor(hexString: "#c9e7ff")
        static let infoBorder = UIColor(hexString: "#1daeeb")
        static let successBackground = UIColor(hexString: "#affdc1")
        static let successBorder = UIColor(hexString: "#208336")
        static let warnBackground = UIColor(hexString: "#fce7a9")
        static let warnBorder = UIColor(hexString: "#a37c07")
        static let errorBackground = UIColor(hexString: "#fdadb5")
        static let errorBorder = UIColor(hexString: "#91232d")
    }
    
}

// Fonts
public enum FontSize {
    case small
    case body
    case bodyBold
    case heading
}

extension FontSize: RawRepresentable {
    public typealias RawValue = CGFloat
    
    public init?(rawValue: RawValue) {
        switch rawValue {
        case 11.0: self = .small
        case 14.0: self = .body
        case 16.0: self = .bodyBold
        case 20.0: self = .heading
        default: return nil
        }
    }
    
    public var rawValue: RawValue {
        switch self {
        case .small: return 11.0
        case .body: return 14.0
        case .bodyBold: return 16.0
        case .heading: return 18.0
        }
    }
}

public enum FontBook: String {
    case regular = "SegoeUI"
    case bold = "SegoeUI-Bold"
    
    func of(size: FontSize) -> UIFont {
        return UIFont(name: self.rawValue, size: size.rawValue)!
    }
}
