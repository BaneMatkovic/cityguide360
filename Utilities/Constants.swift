//
//  Constants.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

let apiUrl = "https://staging.60seconds.app/api/v1"   //staging testing
let googleApiKey = "AIzaSyB_cZLFxUgHwFS2CWuwgfAUBNiKuD0kLj8"
var shareUrl = "www.cityguide360.ba"
let feedbackEmail = "bane.matkovic@gmail.com"
var category = ""
let mainHexColor = "#121925"

let locales = [
    "en": "English",
    "de": "Deutchland",
    "ba": "Bosnian"
]

// categories
let aerialCat = "995ef76ef2038fa803230001"
let foodAndDrinkCat = "995ef76ef2038fa803230002"
let realEstatesCat = "995ef76ef2038fa803230003"
let transportCat = "995ef76ef2038fa803230005"
let shopingCat = "995ef76ef2038fa803230004"
let sportAndRecreationCat = "995ef76ef2038fa803230006"
let servicesCat = "995ef76ef2038fa803230007"
let hotelCat = "995ef76ef2038fa803230008"
let nightLifeCat = "995ef76ef2038fa803230010"
let artAndCultureCat = "995ef76ef2038fa803230011"
let tourismAndAtractionCat = "995ef76ef2038fa803230009"
let generalCat = "995ef76ef2038fa803230012"

//citites
let cities: [String] = ["TUZLA"]

let tuzlaCity = City(name: "TUZLA", lat: 44.538040, long: 18.672180)
