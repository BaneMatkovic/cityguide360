//
//  SearchTextField.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 18/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation
import UIKit

class SearchTextField: UITextField {
    
    var insets:UIEdgeInsets!
    
    init(frame: CGRect = .zero, placeholder:String = "Search", radius:CGFloat = 4, inset:CGFloat = 4) {
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.placeholder = placeholder
        self.setupPlaceholder(with: Theme.Colors.secondaryTextColor, font: UIFont.systemFont(ofSize: 14))
        self.backgroundColor = .white
        self.layer.cornerRadius = radius
        //self.insets = UIEdgeInsets(top: 5, left: inset, bottom: 0, right: 0)
        self.alpha = 0.0
        self.textColor = .gray
        self.textAlignment = .left
        self.keyboardAppearance = .dark
        self.clearButtonMode = .whileEditing
        self.keyboardType = .default
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.returnKeyType = .search
        self.font = UIFont.systemFont(ofSize: 14)
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
