//
//  SpinnerButton.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 23/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class SpinnerButton: UIButton {
    
    // MARK: - Properties
    var spinner: NVActivityIndicatorView!
    var normalTitle: String?
    var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    // MARK: - Designated Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }
    
    // MARK: - View lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if cornerRadius == 0.0 {
            layer.cornerRadius = bounds.size.height/2
        }
        
        let size = bounds.size.height - 10.0
        spinner.addConstraints(width: size, height: size)
    }
    
    // MARK: - Public API
    func startAnimating() {
        spinner.startAnimating()
        isEnabled = false
        
        setTitle("", for: .normal)
        setImage(nil, for: .normal)
    }
    
    func stopAnimating() {
        spinner.stopAnimating()
        isEnabled = true
        
        setTitle(normalTitle, for: .normal)
        setImage(image(for: .normal), for: .normal)
    }
    
    // MARK: - Private API
    fileprivate func configure() {
        //backgroundColor = Theme.Colors.primaryDark
        
        setTitleColor(.white, for: .normal)
        normalTitle = title(for: .normal)
        
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 1
        layer.shadowColor = UIColor.blue.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        spinner = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: .white, padding: 0.0)
        spinner.stopAnimating()
        addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.alignCenterX()
        spinner.alignCenterY()
        
    }
    
}
