//
//  CIty.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 14/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

class City {
    
    var name: String
    var lat: Double
    var long: Double
    
    init(name: String, lat: Double, long: Double) {
        self.name = name
        self.lat = lat
        self.long = long
    }
    
    
}
