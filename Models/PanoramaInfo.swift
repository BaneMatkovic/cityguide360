//
//  PanoramaInfo.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 16/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

class PanoramaInfo {
    
    // MARK: Properties
    
    var name: String
    var kontaktTelefon: String
    var kontaktEmail: String
    var website: String
    var thumbnail: String
    var description: String
    var descriptionEN: String
    var descriptionDE: String
    var radnoVrijeme: String
    var radnoVrijemeEN: String
    var radnoVrijemeDE: String
    
    // MARK: Initializers
    
    init(name: String, kontaktTelefon: String, kontaktEmail: String, website: String, thumbnail: String, description: String, descriptionEN: String, descriptionDE: String, radnoVrijeme: String, radnoVrijemeEN: String, radnoVrijemeDE: String) {
        self.name = name
        self.kontaktTelefon = kontaktTelefon
        self.kontaktEmail = kontaktEmail
        self.website = website
        self.thumbnail = thumbnail
        self.description = description
        self.descriptionEN = descriptionEN
        self.descriptionDE = descriptionDE
        self.radnoVrijeme = radnoVrijeme
        self.radnoVrijemeEN = radnoVrijemeEN
        self.radnoVrijemeDE = radnoVrijemeDE
    }
    
}
