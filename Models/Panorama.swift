//
//  Panorama.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 26/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

struct Panorama: Codable {
    
    // MARK: Properties
    
    var category: String
    var name : String
    var sound: Sound
    var lat: Float
    var lon: Float
    var thumbnail: String
    
    // MARK: Initializers
    
    init(category: String = "", name: String = "", sound: Sound, lat: Float = 0, lon: Float = 0, thumbnail: String = "") {
        self.category = category
        self.name = name
        self.sound = sound
        self.lat = lat
        self.lon = lon
        self.thumbnail = thumbnail
    }
}

struct Sound: Codable {
    var soundBS: String
    var soundEN: String
    var soundDE: String
}
