//
//  Tutorial.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 22/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

struct Tutorial {
    
    // MARK: - Properties
    var title = ""
    var description = ""
    var image: UIImage!
    var color: UIColor!
    
    // MARK: Initializer
    init(title: String, description: String, image: UIImage, color: UIColor) {
        self.title = title
        self.description = description
        self.image = image
        self.color = color
    }
    
}
