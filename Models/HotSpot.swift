//
//  HotSpot.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 14/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

class HotSpot {
    var lat: Double
    var long: Double
    var id: String
    var category: String
    
    init(lat: Double, long: Double, id: String, category: String) {
        self.lat = lat
        self.long = long
        self.id = id
        self.category = category
    }
}
