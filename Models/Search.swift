//
//  Search.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 31/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

struct Search: Codable {
    
    var categoryName: String
    var name: String
    var thumbnail: String
    var id: String
    
    var sound: String?
    var description: String?
    var descriptionEn: String?
    var descriptionDe: String?
}
