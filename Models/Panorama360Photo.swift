//
//  Panorama360Photo.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 17/04/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

struct PanoramaBasic: Codable {
    var id: String?
    var name: String?
    var category: String?
    var thumbnail: String?
    
}

class Panorama360Photo: Codable {
    var id: Int?
    var values: [PanoramaBasic]?
    
    init(id: Int, values: [PanoramaBasic]) {
        self.id = id
        self.values = values
    }
}
