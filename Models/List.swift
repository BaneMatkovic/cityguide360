//
//  List.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 17/03/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

class List: Codable {
    var name: String
    var thumbnail: String
    var panoramaId: String
    
    init(name: String, thumbnail: String, panoramaId: String) {
        self.name = name
        self.thumbnail = thumbnail
        self.panoramaId = panoramaId
    }
}
