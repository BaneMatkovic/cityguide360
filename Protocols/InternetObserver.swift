//
//  InternetObserver.swift
//  CityGuide360
//
//  Created by Branislav Matkovic on 19/02/2019.
//  Copyright © 2019 Branislav Matkovic. All rights reserved.
//

import Foundation

@objc protocol InternetObserver {
    
    
    func setReachabilityNotifier()
    
    @objc func reachabilityChanged(note: Notification)
    
    func didBecomeActiveCheck()
    
}
